package Webinar05;
/*
На вход подается число N — длина массива.
Затем передается массив целых чисел длины N.

Вывести элементы, стоящие на четных индексах массива.
4
20 20 11 13
->
20 11
 */

import java.util.Scanner;

public class Task02 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] myList1 = new int[n];

        for (int i = 0; i < n; i++) {
            myList1[i] = scanner.nextInt();
        }

        for (int i = 0; i < n; i++) {
            if (i % 2 == 0) {
                System.out.print(myList1[i] + " ");
            }
        }

        //        for (int i = 0; i < n; i = i + 2){
        //            System.out.println("элемент: " + arr[i]);
        //        }

    }
}

