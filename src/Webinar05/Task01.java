package Webinar05;
/*
 На вход подается число N — длина массива.
 Затем передается массив целых чисел длины N.

  Вывести все четные элементы массива.
  Если таких элементов нет, вывести -1.

5
1 2 3 4 5
->
2 4
 */

import java.util.Scanner;

public class Task01 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        int[] myList1 = new int[n];
        for (int i = 0; i < myList1.length; i++) {
            myList1[i] = scanner.nextInt();

        }
        int z = -1;
        for (int i = 0; i < myList1.length; i++) {
            if (myList1[i] % 2 == 0) {
                System.out.print(myList1[i] + " ");
                z = 0;
            }
        }
        if (z == -1) {
            System.out.println(z);

        }
        /* набрать fori
        for (int i = 0; i < ; i++) {

        }
        //////////////Решение////////////////
         boolean flag = false;

        for (int i = 0; i < n; i++) {
            if (arr[i] % 2 == 0) {
                System.out.println("Элемент: " + arr[i]);
                flag = true;
            }
        }
        if (!flag) {
            System.out.println(-1);
        }
         */
    }
}
