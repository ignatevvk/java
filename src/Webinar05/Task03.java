package Webinar05;
/*
На вход подается число N — длина массива.
Затем передается массив целых чисел длины N.

Проверить, является ли он отсортированным массивом строго по убыванию.
Если да, вывести true, иначе вывести false.

5
5 4 3 2 1
->
true

2
43 46
->
false

3
5 5 5
->
false

 */

import java.util.Scanner;

public class Task03 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] myList1 = new int[n];

        for (int i = 0; i < n; i++) {
            myList1[i] = scanner.nextInt();
        }

        boolean flag = false;

        for (int i = 1; i < n; i++) {
            if (myList1[i - 1] > myList1[i]) {
                flag = true;
            }
        }
        System.out.println(flag);

       //Решение через метод
        System.out.println(checkIfArrayDesc(myList1));
    }

    // Решение через метод
    static boolean checkIfArrayDesc(int[] inputArray) {
        for (int i = 0; i < inputArray.length - 1; i++) {
            if (inputArray[i] <= inputArray[i + 1]) {
                return false;
            }
        }
        return true;
    }
}
