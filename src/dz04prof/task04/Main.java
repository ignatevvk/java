package dz04prof.task04;
/*
На вход подается список вещественных чисел. Необходимо отсортировать их по
убыванию.
 */

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<Double> list = new ArrayList<>();
        list.add(0.3);
        list.add(15.0);
        list.add(-230.0);
        list.add(-100.58);
        list.add(-100.48);
        list.add(1000.96);

        list.stream()
                .sorted((o1, o2) -> -o1.compareTo(o2))
                .forEach(System.out::println);
    }
}
