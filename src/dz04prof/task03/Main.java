package dz04prof.task03;
/*
На вход подается список строк. Необходимо вывести количество непустых строк в
списке.
Например, для List.of("abc", "", "", "def", "qqq") результат равен 3.
 */

import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> list = List.of("abc", "", "", "def", "qqq");

        List<String> list1 = list.stream()
                .filter(str -> !str.isEmpty()).toList();

        System.out.println(list1.size());
    }
}
