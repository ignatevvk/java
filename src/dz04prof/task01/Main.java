package dz04prof.task01;
/*
Посчитать сумму четных чисел в промежутке от 1 до 100 включительно и вывести ее на
экран.
 */

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<Integer> list = new ArrayList<>();
        for (int i = 0; i <= 100; i++) {
            list.add(i);
        }

        Integer sum = list.stream()
                .filter(digit -> digit % 2 == 0)
                .reduce(0, Integer::sum);

        System.out.println(sum);
    }
}
