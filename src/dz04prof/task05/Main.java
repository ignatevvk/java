package dz04prof.task05;
/*
На вход подается список непустых строк. Необходимо привести все символы строк к
верхнему регистру и вывести их, разделяя запятой.
Например, для List.of("abc", "def", "qqq") результат будет ABC, DEF, QQQ.

 */

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    public static void main(String[] args) {

        List<String> list = List.of("abc", "def", "qqq");
        AtomicInteger z = new AtomicInteger(list.size());
        list.stream()
                .map(String::toUpperCase)
                .forEach(str -> {
                    int k = z.getAndDecrement() - 1;
                    if (k > 0) {
                        System.out.print(str + ", ");
                    } else {
                        System.out.print(str + ".");
                    }
                });
    }
}
