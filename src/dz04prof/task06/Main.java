package dz04prof.task06;
/*
Дан Set<Set<Integer>>. Необходимо перевести его в Set<Integer>
 */

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        Set<Integer> set1 = new HashSet<>();
        set1.add(1);
        set1.add(2);
        set1.add(3);

        Set<Integer> set2 = new HashSet<>();
        set2.add(4);
        set2.add(5);
        set2.add(3);

        Set<Set<Integer>> finalSet = new HashSet<>();
        finalSet.add(set1);
        finalSet.add(set2);

        Set<Integer> newSet = finalSet.stream().
                flatMap(Set::stream)
                .collect(Collectors.toSet());

        System.out.println(newSet);
    }
}
