package dz04prof.taskDop;
/*
На вход подается две строки.
Необходимо определить, можно ли уравнять эти две строки, применив только одну из трех
возможных операций:
1. Добавить символ
2. Удалить символ
3. Заменить символ
Пример:
“cat” “cats” -> true
“cat” “cut” -> true
“cat” “nut” -> false
 */

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        System.out.println("Проверки на true:");
        System.out.println("Проверка 1: " + comparingStrings("cat", "cats"));
        System.out.println("Проверка 2: " + comparingStrings("cat", "cut"));
        System.out.println("Проверка 3: " + comparingStrings("aaa", "aaa"));
        System.out.println("Проверка 4: " + comparingStrings("aaa", "aaaa"));
        System.out.println("Проверка 5: " + comparingStrings("a", "c"));
        System.out.println();

        System.out.println("Проверки на false:");
        System.out.println("Проверка 1: " + comparingStrings("cat", "nut"));
        System.out.println("Проверка 2: " + comparingStrings("catpp", "catss"));
        System.out.println("Проверка 3: " + comparingStrings("a", "cc"));

    }

    public static boolean comparingStrings(String str1, String str2) {
        if (str1.equals(str2)) {
            return true;
        }

        if (str1.length() == str2.length() || (str1.length() - str2.length() == 1)) {
            List<Character> strCh1 = new ArrayList<>();
            for (int i = 0; i < str1.length(); i++) {
                strCh1.add(str1.charAt(i));
            }

            for (int i = 0; i < str2.length(); i++) {
                for (int j = 0; j < strCh1.size(); j++) {
                    if (str2.charAt(i) == strCh1.get(j)) {
                        strCh1.remove(j);
                        break;
                    }
                }
            }
            return strCh1.size() == 1;

        } else if (str1.length() - str2.length() == -1) {
            List<Character> strCh2 = new ArrayList<>();
            for (int i = 0; i < str2.length(); i++) {
                strCh2.add(str2.charAt(i));
            }

            for (int i = 0; i < str1.length(); i++) {
                for (int j = 0; j < strCh2.size(); j++) {
                    if (str1.charAt(i) == strCh2.get(j)) {
                        strCh2.remove(j);
                        break;
                    }
                }
            }
            return strCh2.size() == 1;
        } else {
            return false;
        }
    }
}

