package dz04prof.task02;
/*
На вход подается список целых чисел. Необходимо вывести результат перемножения
этих чисел.
Например, если на вход передали List.of(1, 2, 3, 4, 5), то результатом должно быть число
120 (т.к. 1 * 2 * 3 * 4 * 5 = 120).
 */

import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<Integer> list = List.of(5, 2, 3, 4, 5);
        Integer result = list.stream()
                .reduce(1, (a, b) -> a * b);
        System.out.println(result);
    }
}
