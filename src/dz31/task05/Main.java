package dz31.task05;

public class Main {
    public static void main(String[] args) {

        String[] week = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};

        DayOfWeek[] weekArr = new DayOfWeek[7];
        byte z = 1;
        for (byte i = 0; i < weekArr.length; i++) {
            weekArr[i] = new DayOfWeek(z, week[i]);
            z++;
        }

        // вывод массива
        for (int i = 0; i < weekArr.length; i++) {
            System.out.println(weekArr[i].getDay() + " " + weekArr[i].getWeek());
        }
    }
}
