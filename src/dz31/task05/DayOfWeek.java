package dz31.task05;

public class DayOfWeek {
    private byte day;
    private String week;

    public DayOfWeek(byte day, String week) {
        this.day = day;
        this.week = week;
    }

    public byte getDay() {
        return day;
    }

    public String getWeek() {
        return week;
    }
}
