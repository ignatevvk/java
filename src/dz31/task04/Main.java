package dz31.task04;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int hours = 10;
        int minutes = 59;
        int seconds = 59;

        //Создадим объект time
        TimeUnit time = new TimeUnit(hours, minutes, seconds);

        // вывод времени в 2-x разных форматах
        time.timeInTheFormat24();
        time.timeInTheFormat12();

        // добавляем к времени объекта доп. время
        time.addTime(13, 2, 2);

        // вывод времени в 2-x разных форматах еще раз.
        time.timeInTheFormat24();
        time.timeInTheFormat12();
    }
}
