package dz31.task04;
/*
Необходимо реализовать класс TimeUnit с функционалом, описанным ниже
(необходимые поля продумать самостоятельно). Обязательно должны быть
реализованы валидации на входные параметры.
Конструкторы:
● Возможность создать TimeUnit, задав часы, минуты и секунды.
● Возможность создать TimeUnit, задав часы и минуты. Секунды тогда
должны проставиться нулевыми.
● Возможность создать TimeUnit, задав часы. Минуты и секунды тогда
должны проставиться нулевыми.
Публичные методы:
● Вывести на экран установленное в классе время в формате hh:mm:ss
● Вывести на экран установленное в классе время в 12-часовом формате
(используя hh:mm:ss am/pm)
● Метод, который прибавляет переданное время к установленному в
TimeUnit (на вход передаются только часы, минуты и секунды).

 */

public class TimeUnit {
    private int hours = 0;
    private int minutes = 0;
    private int seconds = 0;

    public TimeUnit(int hours) {
        if (hours >= 24 || hours < 0) {
            System.out.println("Введено некорректное время (часы)");
            System.exit(-1);
        } else {
            this.hours = hours;
        }
    }

    public TimeUnit(int hours, int minutes) {
        this(hours);
        if (minutes >= 60 || minutes < 0) {
            System.out.println("Введено некорректное время (минуты)");
            System.exit(-1);
        } else {
            this.minutes = minutes;
        }
    }

    public TimeUnit(int hours, int minutes, int seconds) {
        this(hours, minutes);
        if (seconds >= 60 || seconds < 0) {
            System.out.println("Введено некорректное время (секунды)");
            System.exit(-1);
        } else {
            this.seconds = seconds;
        }
    }

    public int getHours() {
        return hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public int getSeconds() {
        return seconds;
    }

    //Вывести на экран установленное в классе время в формате hh:mm:ss
    public void timeInTheFormat24() {
        if (hours < 10) {
            System.out.print("0" + hours + ":");
        } else {
            System.out.print(hours + ":");
        }

        if (minutes < 10) {
            System.out.print("0" + minutes + ":");
        } else {
            System.out.print(minutes + ":");
        }

        if (seconds < 10) {
            System.out.print("0" + seconds);
        } else {
            System.out.print(seconds);
        }
        System.out.println();
    }

    //Вывести на экран установленное в классе время в 12-часовом формате(используя hh:mm:ss am/pm)
    public void timeInTheFormat12() {
        String postfix;

        if (hours > 12) {
            int hours12 = hours - 12;
            if (hours12 < 10) {
                System.out.print("0" + hours12 + ":");
            } else {
                System.out.print(hours12 + ":");
            }
            postfix = " pm";
        } else {
            if (hours < 10) {
                System.out.print("0" + hours + ":");
            } else {
                System.out.print(hours + ":");
            }
            postfix = " am";
        }

        if (minutes < 10) {
            System.out.print("0" + minutes + ":");
        } else {
            System.out.print(minutes + ":");
        }

        if (seconds < 10) {
            System.out.print("0" + seconds + postfix);
        } else {
            System.out.print(seconds + postfix);
        }
        System.out.println();
    }

    // Метод, который прибавляет переданное время к установленному в
    //TimeUnit (на вход передаются только часы, минуты и секунды).
    public void addTime(int hours, int minutes, int seconds) {

        if ((this.seconds + seconds) >= 60) {
            this.seconds = (this.seconds + seconds) - 60;
            this.minutes++;
        } else {
            this.seconds += seconds;
        }

        if ((this.minutes + minutes) >= 60) {
            this.minutes = (this.minutes + minutes) - 60;
            this.hours++;
        } else {
            this.minutes += minutes;
        }

        if ((this.hours + hours) >= 24) {
            this.hours = (this.hours + hours) - 24;
        } else {
            this.hours += hours;
        }
    }
}
