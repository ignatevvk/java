package dz31.task07;

public class Main {
    public static void main(String[] args) {
        double a;
        double b;
        double c;

        // тест на true
        a = 5.2;
        b = 7.6;
        c = 3.1;
        System.out.println(TriangleChecker.triangleValidator(a, b, c));

        // тест на false
        a = 10.1;
        b = 5.2;
        c = 3.1;
        System.out.println(TriangleChecker.triangleValidator(a, b, c));
    }
}
