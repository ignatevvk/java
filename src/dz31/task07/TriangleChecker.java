package dz31.task07;


//утильный класс для проверки возможности составить трёхугольник
public class TriangleChecker {
    public static boolean triangleValidator(double a, double b, double c) {

        if ((a + b > c) && (a + c > b) && (b + c > a)) {
            return true;
        } else {
            return false;
        }
    }
}
