package dz31.task08;

public class Atm {
    private double change;
    private static int numberOfATM = 0;

    public Atm(double change) {
        this.change = change;
        numberOfATM++;
    }

    public double changeRubToUsd(double rubles) {
        return rubles / change;
    }

    public double changeUsdToRub(double dollars) {
        return dollars * change;
    }

    public static int getNumberOfATM() {
        return numberOfATM;
    }
}
