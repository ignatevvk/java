package dz31.task08;

public class Main {
    public static void main(String[] args) {

        Atm atm = new Atm(71);
        System.out.println(atm.changeRubToUsd(1500));
        System.out.println(atm.changeUsdToRub(100));
        System.out.println(Atm.getNumberOfATM());

        System.out.println();

        Atm atm1 = new Atm(89);
        System.out.println(atm1.changeRubToUsd(5000));
        System.out.println(atm1.changeUsdToRub(200));
        System.out.println(Atm.getNumberOfATM());
    }
}
