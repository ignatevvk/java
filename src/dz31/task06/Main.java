package dz31.task06;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        char[] chars = {'H', 'e', 'l', 'l', 'o', ' '};
        String string = "Hello";

        // создаём объект по первому конструктору
        AmazingString str = new AmazingString(chars);
        System.out.println(Arrays.toString(str.getChars()));

        // создаём объект по второму конструктору
        AmazingString str1 = new AmazingString(string);
        System.out.println(Arrays.toString(str1.getChars()));

        // тест на получения элемента строки с индексом 1
        System.out.println(str1.getCharI(1));

        // тест на получение длинны строки
        System.out.println(str1.getStringLength());

        // отобразить строку
        str1.printString();
        System.out.println();

        // тесты на подстроку через массив (Проверка true)
        System.out.println("Tесты на подстроку через массив (Проверка true))");
        char[] charsSub = {'H', 'e', 'l', 'l', 'o'};
        System.out.println(str1.validSubstringCh(charsSub));

        char[] charsSub1 = {'l', 'l', 'o'};
        System.out.println(str1.validSubstringCh(charsSub1));

        char[] charsSub2 = {'o'};
        System.out.println(str1.validSubstringCh(charsSub2));

        // тесты на подстроку через массив (Проверка false)
        System.out.println("Tесты на подстроку через массив (Проверка false))");
        char[] charsSub3 = {'H', 'e', 'l', 'l', 'o', ' ', 'H'};
        System.out.println(str1.validSubstringCh(charsSub3));

        char[] charsSub4 = {'Z', 'l', 'o', ' ', 'H'};
        System.out.println(str1.validSubstringCh(charsSub4));

        char[] charsSub5 = {'Z'};
        System.out.println(str1.validSubstringCh(charsSub5));

        // тесты если подстрока типа String (Проверка true)
        System.out.println("Tесты на подстроку типа String (Проверка true))");
        System.out.println(str1.validSubstringString("He"));
        System.out.println(str1.validSubstringString("o"));
        System.out.println(str1.validSubstringString("Hello"));

        System.out.println("Tесты на подстроку типа String (Проверка false))");
        System.out.println(str1.validSubstringString("Zel"));
        System.out.println(str1.validSubstringString(" "));
        System.out.println(str1.validSubstringString("Hellooooo"));

        AmazingString str2 = new AmazingString("  hello");
        System.out.println(Arrays.toString(str2.getChars()));

        // Удаление пробелов из строки
        str2.deleteSpace();

        // Развернуть строку
        System.out.println("Разворот строки " + Arrays.toString(str1.getChars()));
        str1.reversString();


    }
}
