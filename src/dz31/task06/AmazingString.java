package dz31.task06;


import java.util.Arrays;

public class AmazingString {
    private char[] chars;

    public AmazingString(char[] chars) {
        this.chars = chars;
    }

    public AmazingString(String stringChar) {
        char[] temp = new char[stringChar.length()];
        for (int i = 0; i < stringChar.length(); i++) {
            temp[i] = stringChar.charAt(i);
        }
        this.chars = temp;
    }

    public char[] getChars() {
        return chars;
    }

    //Вернуть i-ый символ строки
    public char getCharI(int num) {
        return chars[num];
    }

    // Вернуть длину строки
    public int getStringLength() {
        return chars.length;
    }

    //  Вывести строку на экран
    public void printString() {
        for (int i = 0; i < chars.length; i++) {
            System.out.print(chars[i]);
        }
    }

    // Проверка, есть ли переданная подстрока в AmazingString (на вход подается массив char).
    public boolean validSubstringCh(char[] arr) {
        boolean flag = false;
        if (arr.length <= chars.length) {
            for (int i = 0; i <= chars.length - arr.length; i++) {
                if (chars[i] == arr[0]) {
                    flag = true;
                    int k = 1;
                    for (int j = i + 1; j < arr.length; j++) {
                        if (arr[k] == chars[j]) {
                            k++;
                            flag = true;
                        } else {
                            flag = false;
                            break;
                        }
                    }
                }
            }
        } else {
            flag = false;
        }
        return flag;
    }

    //Проверка, есть ли переданная подстрока в AmazingString (на вход подается String).
    public boolean validSubstringString(String subString) {
        char[] arr = new char[subString.length()];
        for (int i = 0; i < subString.length(); i++) {
            arr[i] = subString.charAt(i);
        }

        boolean flag = false;
        if (arr.length <= chars.length) {
            for (int i = 0; i <= chars.length - arr.length; i++) {
                if (chars[i] == arr[0]) {
                    flag = true;
                    int k = 1;
                    for (int j = i + 1; j < arr.length; j++) {
                        if (arr[k] == chars[j]) {
                            k++;
                            flag = true;
                        } else {
                            flag = false;
                            break;
                        }
                    }
                }
            }
        } else {
            flag = false;
        }
        return flag;
    }

    //Удалить из строки AmazingString ведущие пробельные символы, если они есть
    public void deleteSpace() {
        int i;
        for (i = 0; chars[i] == ' '; i++) {
        }
        char[] temp = new char[chars.length - i];

        for (int j = 0; j < temp.length; j++) {
            temp[j] = chars[i];
            i++;
        }
        this.chars = temp;
        System.out.println(Arrays.toString(chars));
    }

    // Развернуть строку
    public void reversString() {
        char[] temp = new char[chars.length];
        int j = 0;
        for (int i = chars.length - 1; i >= 0; i--) {
            temp[j] = chars[i];
            j++;
        }
        this.chars = temp;
        System.out.println(Arrays.toString(chars));
    }
}








