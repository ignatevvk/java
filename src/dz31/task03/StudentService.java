package dz31.task03;
/*
 Необходимо реализовать класс StudentService.
У класса должны быть реализованы следующие публичные методы:
● bestStudent() — принимает массив студентов (класс Student из
предыдущего задания), возвращает лучшего студента (т.е. который
имеет самый высокий средний балл). Если таких несколько — вывести
любого.
● sortBySurname() — принимает массив студентов (класс Student из
предыдущего задания) и сортирует его по фамилии.
 */

import dz31.task02.Student;

import java.util.ArrayList;

public class StudentService {

    private Student[] students;

    private StudentService() {

    }

    // Принимает массив студентов и сортирует его по фамилии
    public static Student[] sortBySurname(Student[] students) {
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < students.length; i++) {
            list.add(i, students[i].getSurname());
        }
        list.sort(null);

        Student[] temp = new Student[students.length];
        for (int i = 0; i < temp.length; i++) {
            for (int j = 0; j < students.length; j++) {
                if (students[j].getSurname() == list.get(i)) {
                    temp[i] = students[j];
                }
            }
        }
        return temp;
    }


    // принимает массив студентов (класс Student из предыдущего задания),
    // возвращает лучшего студента (т.е. который имеет самый высокий средний балл).
    public static Student bestStudent(Student[] students) {
        double maxBall = 0;
        int z = 0;
        for (int i = 0; i < students.length; i++) {
            if (students[i].averageScore() > maxBall) {
                maxBall = students[i].averageScore();
                z = i;
            }
        }
        return students[z];
    }
}
