package dz31.task03;

import dz31.task02.Student;

public class Main {
    public static void main(String[] args) {

        Student[] students = new Student[3];

        Student student0 = new Student();
        students[0] = student0;
        student0.setName("Иван");
        student0.setSurname("Иванов");
        student0.setGrades(new int[]{5, 5, 5, 5, 5, 5, 5, 5, 5, 5});

        Student student1 = new Student();
        students[1] = student1;
        student1.setName("Борис");
        student1.setSurname("Борисов");
        student1.setGrades(new int[]{4, 4, 4, 4, 4, 4, 4, 4, 4, 4});

        Student student2 = new Student();
        students[2] = student2;
        student2.setName("Артём");
        student2.setSurname("Артёмов");
        student2.setGrades(new int[]{3, 3, 3, 3, 3, 3, 3, 3, 3, 3});

        //Принимает массив студентов (класс Student из предыдущего задания), возвращает лучшего студента (т.е. который
        //имеет самый высокий средний балл).
        Student bestStudent = StudentService.bestStudent(students);
        System.out.println("Лучший студент: " + bestStudent.getName() + " " + bestStudent.getSurname() +
                ", средний балл: " + bestStudent.averageScore());


        //Принимает массив студентов (класс Student из предыдущего задания) и сортирует его по фамилии.
        students = StudentService.sortBySurname(students);

        //Проверка, что массив отсортированн по фамилии
        for (int i = 0; i < students.length; i++) {
            System.out.println(students[i].getName() + " " + students[i].getSurname());

        }
    }
}
