package dz31.task01;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        Cat cat = new Cat();

        System.out.print("Введите количество операций: ");
        int n = scanner.nextInt();
        for (int i = 1; i <= n; i++) {
            cat.status();
        }
    }
}
