package dz31.task02;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        // Создаеём объект и заполняем поля
        Student student = new Student();
        student.setName("Иван");
        student.setSurname("Иванов");

        // Заполним массив оценок случайными числами от 1 до 5 и передаём объекту
        int[] arr = new int[10];
/*
        for (int i = 0; i < arr.length; i++) {
            arr[i] = ((int) (Math.random() * 5) + 1);
        }
       */

       arr[0] = 3;
       arr[1] = 5;

        // заполним массив оценок передав массив
        student.setGrades(arr);

        System.out.println(student.averageScore());

        // получим данные об объекте (студент)
        System.out.println(student.getName() + " " + student.getSurname());
        System.out.println(Arrays.toString(student.getGrades()));

        // добавим в конец массива оценок 1
        student.newAssessment(3);
        // вывод обновлённого массива смещённого в лево
        System.out.println(Arrays.toString(student.getGrades()));
        // получим средний бал студента
        System.out.println(student.averageScore());
    }
}
