package dz31.task02;
/*
Необходимо реализовать класс Student.
У класса должны быть следующие приватные поля:
● String name — имя студента
● String surname — фамилия студента
● int[] grades — последние 10 оценок студента. Их может быть меньше, но
не может быть больше 10.
И следующие публичные методы:
● геттер/сеттер для name
● геттер/сеттер для surname
● геттер/сеттер для grades
● метод, добавляющий новую оценку в grades. Самая первая оценка
должна быть удалена, новая должна сохраниться в конце массива (т.е.
массив должен сдвинуться на 1 влево).
● метод, возвращающий средний балл студента (рассчитывается как
среднее арифметическое от всех оценок в массиве grades)

 */

import java.util.Arrays;

public class Student {
    private String name;
    private String surname;
    private int[] grades = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    public Student() {

    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int[] getGrades() {
        return grades;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }


    // Добавляет новую оценку в grades смещая массив влево
    public void newAssessment(int assessment) {
        for (int i = 1; i < grades.length; i++) {
            grades[i - 1] = grades[i];
        }
        grades[grades.length - 1] = assessment;
    }

    // Возвращает средний балл студента
    /*
    метод некорректно работает. Если у студента всего две оценки, например, 3 и 5, то его правильный средний балл 4,0, а у вас отдает 0,8
    !сделать цикл
     */
    public double averageScore() {
        double quantity = 0;
        for (int i = 0; i < grades.length; i++) {
            if (grades[i] > 0) {
                quantity++;
            }
        }
        double point = Arrays.stream(grades).sum() / quantity;
        return point;
    }
}
