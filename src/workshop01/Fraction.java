package workshop01;
/*
Создайте класс Fraction с двумя целочисленными final-полями: числителем и знаменателем.
Конструктор этого класса создает дроби при заданных значениях, но выбрасывает исключение NullDenominatorException
при нулевом знаменателе. Для этого дополнительно создайте класс проверяемого исключения NullDenominatorException,
объекты которого выбрасываются, если знаменатель объекта типа Fraction равен 0,
а также напишите тестовый класс, который проверяет эти классы
 */

public class Fraction {

    public final int numerator; // числитель
    public final int denominator; // знаменатель

    public Fraction(int numerator, int denominator) throws NullDenominatorException {

        if (denominator == 0) {
            throw new NullDenominatorException("Знаменатель объекта типа Fraction равен 0");
        }
        this.numerator = numerator;
        this.denominator = denominator;
    }

    @Override // Переопределяет метод toString класса Object
    public String toString() {
        return numerator + "/" + denominator;
    }
}
