package workshop01;

public class TestFraction {
    public static void main(String[] args) throws NullDenominatorException {
        try {
            Fraction fraction = new Fraction(1, 0);
            System.out.println(fraction);
        } catch (NullDenominatorException e) {
            System.out.println(e.getMessage());
        }
    }
}
