package Dz02;
/*
Раз так легко получается разделять по первому пробелу, Петя решил
немного изменить предыдущую программу и теперь разделять строку по
последнему пробелу.
 */

import java.util.Scanner;

public class dz08_ {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String string = scanner.nextLine();
        int index = string.lastIndexOf(' ');

        System.out.print(string.substring(0, index) + "\n" + string.substring(index + 1));
    }
}
