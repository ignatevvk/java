package Dz02;

import java.util.Scanner;

/*
Петя недавно изучил строки в джаве и решил попрактиковаться с ними.
Ему хочется уметь разделять строку по первому пробелу. Для этого он может
воспользоваться методами indexOf() и substring().
На вход подается строка. Нужно вывести две строки, полученные из входной
разделением по первому пробелу.

Входные данные   Выходные данные
Hi great team!   Hi
                 great team!
Hello world!     Hello
                 world!
 */
public class dz07_ {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String string = scanner.nextLine();
        int index = string.indexOf(' ');

        System.out.print(string.substring(0, index) + "\n" + string.substring(index + 1));
    }
}
