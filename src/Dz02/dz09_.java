package Dz02;
/*
Пока Петя практиковался в работе со строками, к нему подбежала его
дочь и спросила: "А правда ли, что тригонометрическое тождество (sin^2(x)+
cos^2(x) - 1 == 0) всегда-всегда выполняется?"
Напишите программу, которая проверяет, что при любом x на входе
тригонометрическое тождество будет выполняться (то есть будет выводить true
при любом x).

Входные данные Выходные данные
90             true
0              true
-200           true
 */

import java.util.Scanner;

public class dz09_ {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        double x = scanner.nextInt();

        System.out.println(Math.pow(Math.sin(Math.toRadians(x)), 2) + Math.pow(Math.cos(Math.toRadians(x)), 2) == 1);
    }
}
