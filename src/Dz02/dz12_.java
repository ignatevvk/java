package Dz02;
/*
У Марата был взломан пароль. Он решил написать программу,
которая проверяет его пароль на сложность. В интернете он узнал, что пароль
должен отвечать следующим требованиям:
- пароль должен состоять из хотя бы 8 символов;
- в пароле должны быть:
○ заглавные буквы
○ строчные символы
○ числа
○ специальные знаки(_*-)
Если пароль прошел проверку, то программа должна вывести в консоль строку пароль
надежный, иначе строку: пароль не прошел проверку.
Входные данные Выходные данные
Hello_22       пароль надежный
world234       пароль не прошел проверку
 */

import java.util.Scanner;

public class dz12_ {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String pass = scanner.nextLine();

        int i;
        char ch;
        int p1, p2, p3, p4;
        p1 = 0;
        p2 = 0;
        p3 = 0;
        p4 = 0;

        //получим длину строки
        int longString = pass.length();

        if (longString >= 8) {
            for (i = 0; i < longString; ++i) {
                ch = pass.charAt(i);
                if (ch >= 'A' && ch <= 'Z')
                    p1 = 1;
                else if (ch >= 'a' && ch <= 'z')
                    p2 = 1;
                else if (ch >= '0' && ch <= '9')
                    p3 = 1;
                else if (ch == '_' || ch == '*' || ch == '-')
                    p4 = 1;
            }
            if (p1 + p2 + p3 + p4 == 4) {
                System.out.println("пароль надежный");
            } else {
                System.out.println("пароль не прошел проверку");
            }
        } else {
            System.out.println("пароль не прошел проверку");
        }
    }
}





