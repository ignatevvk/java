package Dz02;
/*
Давай проверим, можно ли из трех сторон составить треугольник?".
На вход подается три целых положительных числа – длины сторон
треугольника. Нужно вывести true, если можно составить треугольник из этих
сторон и false иначе.

a+b>c, a+c>b, b+c>a, (a>0, b>0, c>0)

Входные данные Выходные данные
3 2 1          false
3 4 5          true
2 15 15        true
10 2 7         false
 */

import java.util.Scanner;

public class dz11_ {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        System.out.println(a + b > c && a + c > b && b + c > a);
    }
}
