package Dz02;

import java.util.Scanner;

/*
После вкусного обеда Петя принимается за подсчет дней до выходных.
Календаря под рукой не оказалось, а если спросить у коллеги Феди, то тот
называет только порядковый номер дня недели, что не очень удобно. Поэтому
Петя решил написать программу, которая по порядковому номеру дня недели
выводит сколько осталось дней до субботы. А если же сегодня шестой
(суббота) или седьмой (воскресенье) день, то программа выводит "Ура,
выходные!"

Входные данные Выходные данные
5              1
7              Ура, выходные!
3              3
1              5
 */
public class dz04_ {
    public static final int DAYS_FOR_SATURDAY = 6;

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int day = scanner.nextInt();

        if (day >= DAYS_FOR_SATURDAY) {
            System.out.println("Ура, выходные!");
        } else {
            System.out.println(DAYS_FOR_SATURDAY - day);
        }
    }
}

