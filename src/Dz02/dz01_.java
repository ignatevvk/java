package Dz02;

/*
(1 балл) За каждый год работы Петя получает на ревью оценку. На вход
подаются оценки Пети за последние три года (три целых положительных числа).
Если последовательность оценок строго монотонно убывает, то вывести "Петя,
пора трудиться"
В остальных случаях вывести "Петя молодец!"

Входные данные Выходные данные
10 5 2         Петя, пора трудиться
4 20 15        Петя молодец!
5 5 5          Петя молодец!
 */

import java.util.Scanner;

public class dz01_ {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        if (a > b && b > c) {
            System.out.println("Петя, пора трудиться");
        }
        else {
            System.out.println("Петя молодец!");
        }

    }

}
