package Dz02;
/*
(1 балл) Петя снова пошел на работу. С сегодняшнего дня он решил ходить на
обед строго после полудня. Периодически он посматривает на часы (x - час,
который он увидел). Помогите Пете решить, пора ли ему на обед или нет. Если
время больше полудня, то вывести "Пора". Иначе - “Рано”.

Входные данные Выходные данные
7              Рано
13             Пора
12             Рано
22             Пора

 */

import java.util.Scanner;

public class dz03_ {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        if (x <= 12) {
            System.out.println("Рано");

        } else {
            System.out.println("Пора");
        }
    }
}


