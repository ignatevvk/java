package Dz02;
/*
 "А логарифмическое?" - не унималась дочь.
Напишите программу, которая проверяет, что log(e^n) == n для любого
вещественного n

Входные данные Выходные данные
1,0            true
12,34          true
-42,10         true
 */

import java.util.Scanner;

public class dz10_ {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        double n = scanner.nextDouble();
        System.out.println(Math.log(Math.pow(Math.E, n)) == n);
    }
}
