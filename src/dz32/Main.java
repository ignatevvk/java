package dz32;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        Library myLibrary = new Library();

        myLibrary.addBook("Изучаем Java", "Кэти Сьерра");
        myLibrary.addBook("Java. Руководство для начинающих", "Герберт Шилдт");
        myLibrary.addBook("Java для чайников", "Барри Бёрд");
        myLibrary.addBook("Java. Полное руководство", "Герберт Шилдт");

        myLibrary.listBooks();

        Visitor visitor1 = new Visitor("Иванов");
        Visitor visitor2 = new Visitor("Петров");

        //Тест 1
        System.out.println();
        System.out.println("Тест-1");
        // найдём нужную книгу
        Book book1 = myLibrary.searchBook("Изучаем Java");
        // выдадим эту книгу Иванову
        myLibrary.takeBook(book1, visitor1);
        myLibrary.listBooks();
        System.out.println("Имя: " + visitor1.getNameVisitor() + " ID: " + visitor1.getIdVisitor() + " читает: "
                + visitor1.getReadeBook().getNameBook());

        //Тест 2 Попытаемся добавить или удалить выданную книгу
        System.out.println();
        System.out.println("Тест-2");
        myLibrary.addBook("Изучаем Java", "Кэти Сьерра");
        myLibrary.deleteBook("Изучаем Java");
        myLibrary.listBooks();

        //Тест 3 Найти и вернуть книгу по названию.
        System.out.println();
        System.out.println("Тест-3");
        myLibrary.searchBook("JavaTest");
        Book book2 = myLibrary.searchBook("Java для чайников");
        System.out.println(book2.getNameBook() + " " + book2.getWriterBook());

        //Тест 4 Найти и вернуть список книг по автору
        System.out.println();
        System.out.println("Тест-4");
        ArrayList<Book> booksW = myLibrary.searchWriterBook("Герберт Шилдт");
        for (Book book : booksW) {
            System.out.println(book.getNameBook() + " " + book.getWriterBook());
        }

        //Тест 5 Пытаемся выдать выданую книгу
        System.out.println();
        System.out.println("Тест-5");
        myLibrary.takeBook(book1, visitor2);
        myLibrary.takeBook("Java", visitor2);
        myLibrary.takeBook("Java для чайников", visitor2);
        myLibrary.takeBook("Java. Полное руководство", visitor2);

        //Тест 6 возврат книги
        System.out.println();
        System.out.println("Тест-6");
        Visitor visitor3 = new Visitor("Сидоров");
        myLibrary.returnBook(book1, visitor3, 4);
        myLibrary.returnBook(book1, visitor1,5);

        // Тест 7 получение средней оценки по книги
        System.out.println();
        System.out.println("Тест-7");
        myLibrary.takeBook(book1, visitor3);
        myLibrary.returnBook(book1, visitor3, 4);
        myLibrary.takeBook(book1, visitor3);
        myLibrary.returnBook(book1, visitor3, 4);
        System.out.printf("%.2f%n",book1.getAverageScore());
        System.out.println(book2.getAverageScore());
    }
}
