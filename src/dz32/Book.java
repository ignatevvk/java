package dz32;

import java.util.ArrayList;

public class Book {
    private String nameBook;
    private String writerBook;
    private boolean rent;

    private ArrayList<Integer> scores;

    public Book(String nameBook, String writerBook) {
        this.nameBook = nameBook;
        this.writerBook = writerBook;
        this.rent = false;
        this.scores = new ArrayList<>();
    }

    public String getNameBook() {
        return nameBook;
    }

    public String getWriterBook() {
        return writerBook;
    }

    public boolean isRent() {
        return rent;
    }

    public void setRent(boolean rent) {
        this.rent = rent;
    }

    public void addScore(Integer score) {
        scores.add(score);
    }

    public double getAverageScore() {
        if (scores.isEmpty()) {
            return 0;
        } else {
            int sum = 0;
            for (Integer num : scores) {
                sum += num;
            }
            return (double) sum / scores.size();
        }
    }
}
