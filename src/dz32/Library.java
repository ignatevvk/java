package dz32;

import java.util.ArrayList;
import java.util.Objects;

public class Library {
    private ArrayList<Book> bookArr;
    private int numId = 0;

    public Library() {
        this.bookArr = new ArrayList<>();
    }

    /*
    Добавить новую книгу в библиотеку, если книги с таким наименованием ещё нет
    в библиотеке. Если книга в настоящий момент одолжена, то считается, что она
    всё равно есть в библиотеке (просто в настоящий момент недоступна).
   */
    public void addBook(String nameBook, String writerBook) {
        boolean flag = true;
        for (Book book : bookArr) {
            if (Objects.equals(book.getNameBook(), nameBook)) {
                flag = false;
                System.out.println("Такая книга уже есть!");
                break;
            }
        }
        if (flag) {
            bookArr.add(new Book(nameBook, writerBook));
        }
    }

    /*
    Удалить книгу из библиотеки по названию, если такая книга в принципе есть в
    библиотеке и она в настоящий момент не одолжена.
     */
    public void deleteBook(String nameBook) {
        boolean flag = true;
        for (int i = 0; i < bookArr.size(); i++) {
            if (Objects.equals(bookArr.get(i).getNameBook(), nameBook) && !bookArr.get(i).isRent()) {
                bookArr.remove(i);
                flag = false;
                break;
            }
        }
        if (flag) {
            System.out.println("Такой книги нет!");
        }
    }

    //Вывод списка книг
    public void listBooks() {
        for (Book book : bookArr) {
            System.out.println(book.getNameBook() + " " + book.getWriterBook() + " в наличии: " + !(book.isRent()));
        }
    }

    // Проверка на наличия книги с заданным названием (возвращвет книгу)
    public Book searchBook(String nameBook) {
        for (Book book : bookArr) {
            if (Objects.equals(book.getNameBook(), nameBook)) {
                return book;
            }
        }
        System.out.println("Такой книги нет!");
        return null;
    }

    //Найти и вернуть список книг по автору.
    public ArrayList<Book> searchWriterBook(String writerBook) {
        ArrayList<Book> temp = new ArrayList<>();
        for (Book book : bookArr) {
            if (Objects.equals(book.getWriterBook(), writerBook)) {
                temp.add(book);
            }
        }
        return temp;
    }

    //взять книгу (если передаем объект)
    public void takeBook(Book book, Visitor reader) {
        if (reader.getIdVisitor() == 0) {
            reader.setIdVisitor(++numId);
        }

        if (book == null) {
            System.out.println("Такой книги нет.");
        } else if (reader.getReadeBook() != null) {
            System.out.println("У вас уже есть книга");
        } else if (book.isRent()) {
            System.out.println("Книга уже одолжена");
        } else {
            book.setRent(true);
            reader.setReadeBook(book);
            System.out.println("Вы взяли книгу.");
        }
    }

    //взять книгу (если передаем строку)
    public void takeBook(String nameBook, Visitor reader) {
        Book bookTemp = null;
        for (Book book : bookArr) {
            if (Objects.equals(book.getNameBook(), nameBook)) {
                bookTemp = book;
            }
        }

        if (reader.getIdVisitor() == 0) {
            reader.setIdVisitor(++numId);
        }

        if (bookTemp == null) {
            System.out.println("Такой книги нет.");
        } else if (reader.getReadeBook() != null) {
            System.out.println("У вас уже есть книга");
        } else if (bookTemp.isRent()) {
            System.out.println("Книга уже одолжена");
        } else {
            bookTemp.setRent(true);
            reader.setReadeBook(bookTemp);
            System.out.println("Вы взяли книгу.");
        }
    }

    public void returnBook(Book book, Visitor reader, Integer score) {
        if (reader.getReadeBook() == book) {
            book.setRent(false);
            book.addScore(score);
            reader.setReadeBook(null);
            System.out.println("Вы вернули книгу.");
        } else {
            System.out.println("Вы пытаетесь вернуть чужую книгу");
        }
    }
}








