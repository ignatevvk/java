package dz32;

public class Visitor {
    private String nameVisitor;
    private int idVisitor;
    private Book readeBook;

    public Visitor(String nameVisitor) {
        this.nameVisitor = nameVisitor;
        this.readeBook = null;
        this.idVisitor = 0;
    }

    public String getNameVisitor() {
        return nameVisitor;
    }

    public int getIdVisitor() {
        return idVisitor;
    }

    public Book getReadeBook() {
        return readeBook;
    }

    public void setReadeBook(Book readeBook) {
        this.readeBook = readeBook;
    }

    public void setIdVisitor(int idVisitor) {
        this.idVisitor = idVisitor;
    }
}
