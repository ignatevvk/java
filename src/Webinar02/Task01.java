package Webinar02;

import java.util.Scanner;

/*
Дано число n. Нужно проверить четное ли оно.
*/

public class Task01 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String str;
        /*if (n % 2 == 0) {
            str = "число четное";
        } else {
            str = "число нечётное";

        }
        */
        str = (n % 2 == 0) ? "число четное" : "число нечётное";
        System.out.println(n + str);
    }
}
