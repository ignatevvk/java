package Webinar02;

import java.util.Scanner;

public class Task09 {

    /*
   Дана строка и паттерн, заменить паттерн на паттерн, состоящий из заглавных символов
   Входные данные
   Hello
   o
   Выходные данные
   HellO

   Входные данные
   Hello world
   ld
   Выходные данные
   Hello worLD
    */
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        String pattern = scanner.next();

        String upperPattern = pattern.toUpperCase();
        System.out.println(str.replace(pattern, upperPattern));
    }
}




