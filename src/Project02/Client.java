package Project02;

public class Client {
    public static void main(String[] args) {

        int id = 1122;
        int ballance = 20000;
        double stavka = 4.5;

        // создаём объект счёт
        Account account = new Account(id, ballance);

        // укажим годовую ставку
        Account.setAnnualInterestRate(stavka);

        //снимаем деньги
        account.withdraw(2500);

        // пополняем счёт
        account.deposit(3000);

        System.out.println("Баланс равен " + account.getBalance() + " руб.");
        System.out.println("Ежемесячный процент равен " + account.getMonthlyInterest() + " руб.");
        System.out.println("Этот счет был создан " + account.getDateCreated());
    }
}
