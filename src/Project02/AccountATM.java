package Project02;

import java.util.Scanner;

public class AccountATM {
    private static Scanner input = new Scanner(System.in);

    public Account[] accounts = new Account[10];

    //Конструктор с заполнием массива банковскими счетами с идентификаторами от 0 до 9
    // и начальным балансом 10 000 рублей
    public AccountATM() {
        for (int i = 0; i < accounts.length; i++) {
            accounts[i] = new Account(i, 10000);
        }
    }

    public int getAccountsLength() {
        return accounts.length;
    }

    // Метод, который отображает меню и получает один из его пунктов
    public int getMenuSelection() {
        int choice;
        while (true) {
            System.out.println("\nОсновное меню");
            System.out.println("1: проверить баланс счета");
            System.out.println("2: снять со счета");
            System.out.println("3: положить на счет");
            System.out.println("4: выйти");
            System.out.print("Введите пункт меню: ");
            choice = input.nextInt();
            if (choice < 1 || choice > 4) {
                System.out.println("Вы ввели некорректный пункт меню!");
            } else {
                break;
            }
        }
        return choice;
    }

    //Метод, который снимает с выбранного счета указанную сумму
    public void withdraw(int id) {
        System.out.print("Введите сумму для снятия со счета: ");
        double amount = input.nextDouble();
        if (amount < 0) {
            System.out.print("Сумма отрицательная! Операция отменена.");
        } else if (amount <= accounts[id].getBalance()) {
            accounts[id].withdraw(amount);
        } else {
            System.out.print("Недостаточно средств на счете! Операция отменена.");
        }
    }

    //Метод, который пополняет выбранный счет на указанную сумму
    public void deposit(int id) {
        System.out.print("Введите сумму для внесения на счет: ");
        double amount = input.nextDouble();
        if (amount >= 0) {
            accounts[id].deposit(amount);
        } else {
            System.out.print("Сумма отрицательная! Операция отменена.");
        }
    }
}

