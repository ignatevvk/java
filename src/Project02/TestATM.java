package Project02;

import java.util.Scanner;

public class TestATM {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        AccountATM accountATM = new AccountATM();
        int id;

        while (true) {
            // обработку ввода идентификатора счета
            System.out.print("Введите id: ");
            id = input.nextInt();
            if (id < 0 || id > accountATM.getAccountsLength()) {
                System.out.println("Введите, пожалуйста, корректный id");
                continue;
            }

            //цикл по выполнению действия с выбранным счетом
            while (true) {
                int choice = accountATM.getMenuSelection();
                if (choice == 1)
                    System.out.println("Баланс равен " + accountATM.accounts[id].getBalance());
                else if (choice == 2)
                    accountATM.withdraw(id);
                else if (choice == 3)
                    accountATM.deposit(id);
                else
                    break;
            }
        }
    }
}
