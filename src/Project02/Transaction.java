package Project02;

import java.util.Date;

public class Transaction {

    private Date date;
    /**
     * Тип транзакции, где + пополнение, - списание
     */
    private char type;

    private double amount;
    private double balance;
    private String description;

    //конструктор с четырьмя параметрами, значение дата транзакции определяется текущей датой и временем
    public Transaction(char type, double amount, double balance, String description) {
        date = new Date();
        this.type = type;
        this.amount = amount;
        this.balance = balance;
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public char getType() {
        return type;
    }

    // возвращвет сумму транзвкции
    public double getAmount() {
        return amount;
    }

    // возврашает балланс после выполнения транзакции
    public double getBalance() {
        return balance;
    }

    //возвращает описание транзакции
    public String getDescription() {
        return description;
    }


}
