package Project02;

import java.util.ArrayList;
import java.util.Date;

public class Account {

    private int id;                              // id счёта
    private double balance;                     // остаток, балланс
    private static double annualInterestRate;  // годовая процентная ставка
    private Date dateCreated;                  // дата создания счета

    private String name;                      // имя владельца счета

    private ArrayList<Transaction> transactions = new ArrayList<>();      // список будет хранить объекты типа Transaction

    // конструктор без параметров
    public Account() {
        dateCreated = new Date();
    }

    //конструктор, в котором будут определяться значения полей id и balance
    public Account(int id, double balance) {
        this.id = id;
        this.balance = balance;
        dateCreated = new Date();
    }

  // конструктор с параметрами
    public Account(String name, int id, double balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
        dateCreated = new java.util.Date();
    }


    // Возвращает id
    public int getId() {
        return id;
    }

    // Возвращает баланс
    public double getBalance() {
        return balance;
    }

    // Возвращает годовую процентную ставку
    public static double getAnnualInterestRate() {
        return annualInterestRate;
    }

    // Возвращает дату создания счета
    public Date getDateCreated() {
        return dateCreated;
    }


    // Присваивает новый id
    public void setId(int id) {
        this.id = id;
    }

    // Присваивает новый баланс
    public void setBalance(double balance) {
        this.balance = balance;
    }

    // Присваивает новую годовую процентную ставку
    public static void setAnnualInterestRate(double annualInterestRate) {
        Account.annualInterestRate = annualInterestRate;
    }

    /*
    Метод, который возвращает ежемесячный процен
    метод getMonthlyInterest() предназначен для возврата ежемесячных процентов, а не процентной ставки.
    Ежемесячные проценты = balance * ежемесячная процентная ставка.
    Ежемесячная процентная ставка = annualInterestRate / 12.
    Обратите внимание, что значение annualInterestRate измеряется в процентах, например, 4.5%,
    поэтому необходимо делить его на 100.)
     */
    public double getMonthlyInterest() {
        return balance * (annualInterestRate / 1200);
    }

    public String getName() {
        return name;
    }

    public ArrayList<Transaction> getTransactions() {
        return transactions;
    }


    //метод, который снимает со счета указанную сумму
    public void withdraw(double amount) {
        balance -= amount;
        transactions.add(new Transaction('-', amount, balance, "")); // новая транзакция добавлялась в список транзакций
    }

    // метод, который пополняет счет на указанную сумму
    public void deposit(double amount) {
        balance += amount;
        transactions.add(new Transaction('+', amount, balance, "")); // новая транзакция добавлялась в список транзакций
    }
}

