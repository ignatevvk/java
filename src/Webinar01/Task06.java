package Webinar01;

// Дано двухзначное число. Вывесли сначала левую цифру (единици), затем правую (десятки)

import java.util.Scanner;

public class Task06 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int c = scanner.nextInt();
        System.out.println("Результат");
        System.out.println("Единицы: " + c % 10 + ";Десятки: " + c / 10);
    }

}
