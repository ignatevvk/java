package Webinar01;

import java.util.Scanner;

public class Task04 {
    /*

Дана площадь круга, нужно найти диаметр окружности и длину окружности.
S  = PI * (D^2 / 4) - это через диаметр => d = sqrt(S * 4 / PI)
S = PI * r^2 - радиус
S = L^2 / (4 *PI) - площадь через длину
Отношение длины окружности к диаметру является постоянным числом.
π = L : d

Входные данные:
91
 */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double s = scanner.nextDouble();
        double d = Math.sqrt(s * 4 / Math.PI);
        double l = Math.PI * d;

        System.out.println("Результат работы программы D: " + d + " длинна" + l);
    }
}
