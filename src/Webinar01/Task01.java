package Webinar01;

import java.util.Scanner;

public class Task01 {

    /*
    Даны числа a, b, c. Нужно перенести значения из a -> b, из b -> с, и из c -> а.

    Входные данные:
    a = 3, b = 2, c = 1.
     */

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        System.out.println("Результат ввода: " + a + " " + b + " " + c);
        int temp = c;
        c = b;
        b = a;
        a = temp;
        //System.out.println("Результат ввода: "+ a + " " + b + " "+ c);
        System.out.printf(" a=%s, b=%s, c=%s ", a, b, c);
    }

}
