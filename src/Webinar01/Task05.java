package Webinar01;

import java.util.Scanner;

public class Task05 {
    /* Написать аналог функции swap, которая меняет значение параметров местами (без вспомогательной переменной)
       Входные данный
       a=8; b=10; */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        System.out.println("Входные данный");
        System.out.println("a=" + a + "; b=" + b);

        // Логика
        a = a + b;//2 + 5 = 7 -> a = 7
        b = a - b;//7 - 5 = 2 -> b = 2
        a = a - b;//7 - 2 = 5 -> a = 5

        System.out.println("Результат");
        System.out.println("a=" + a + ";b=" + b);
    }
}
