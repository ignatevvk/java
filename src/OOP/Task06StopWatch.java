package OOP;
/*
Напишите клиент этого класса – программу, которая вычисляет время выполнения сортировки 100 000 чисел методом выбора.
 */

import java.util.Random;

public class Task06StopWatch {
    public static void main(String[] args) {

        int n = 100000;
        Random generator = new Random(1000);

        double[] list = new double[n];
        for (int i = 0; i < list.length; i++) {
            list[i] = generator.nextInt(1000);
        }

        StopWatch myTime = new StopWatch();

       // myTime.start();

        /**
         * Сортирует массив методом выбора
         */

        for (int i = 0; i < list.length - 1; i++) {
            // Найти наименьшее значение в list[i..list.length-1]
            double currentMin = list[i];
            int currentMinIndex = i;

            for (int j = i + 1; j < list.length; j++) {
                if (currentMin > list[j]) {
                    currentMin = list[j];
                    currentMinIndex = j;
                }
            }

            // Переставить list[i] и list[currentMinIndex], если необходимо
            if (currentMinIndex != i) {
                list[currentMinIndex] = list[i];
                list[i] = currentMin;
            }

        }
        for (int i = 0; i < list.length; i++) {

            System.out.println(list[i]);
        }
        StopWatch myTime1 = new StopWatch(); // Второй объект

        myTime.stop();

        System.out.println(myTime.getStartTime());
        System.out.println(myTime.getEndTime());

        System.out.println(myTime.getElapsedTime());
    }
}




