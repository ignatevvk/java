package OOP;
/*
Напишите клиент этого класса — программу, которая создает три объекта типа MyTime (с помощью new MyTime(),
new MyTime(555550000) и new MyTime(5, 23, 55)) и отображает значениях их полей данных hour, minute и second
в формате часы:минуты:секунды.

(Подсказка: первые два конструктора извлекут значения hour, minute и second из прошедшего времени.
Для безаргументного конструктора текущее время может быть получено с помощью метода System.currentTimeMillis(),
как было показано в программе ShowCurrentTime из курса «Основы Java-программирования». Пусть время будет GMT.)
 */

public class Task08MyTime {
    public static void main(String[] args) {
        MyTime time1 = new MyTime();
        System.out.println(time1.getHour() + ":" + time1.getMinute() + ":" + time1.getSecond());

        MyTime time2 = new MyTime(555550000);
        System.out.println(time2.getHour() + ":" + time2.getMinute() + ":" + time2.getSecond());

        MyTime time3 = new MyTime(5, 23, 55);
        System.out.println(time3.getHour() + ":" + time3.getMinute() + ":" + time3.getSecond());
    }
}
