package OOP;
/*
Напишите клиент этого класса — программу, которая создает два объекта типа
MyDate (с помощью new MyDate() и new MyDate(34355555133101L)) и отображает их год, месяц и день.

(Подсказка: первые два конструктора извлекут год, месяц и день из прошедшего времени.
Например, если прошедшее время составляет 561555550000 миллисекунд, то год равен 1987, месяц равен 9,
а день равен 18. Для упрощения кодирования можно использовать класс GregorianCalendar,
описанный в подразделе «Задания. Часть 2» этого курса.)
 */

public class Task10MyDate {
    public static void main(String[] args) {
        MyDate date1 = new MyDate();
        MyDate date2 = new MyDate(34355555133101L);

        System.out.println(date1.getYear() + " " + dateNew(date1.getMonth()) + " " + date1.getDay());
       // System.out.println(date1.getYear() + " " + date1.getMonth() + " " + date1.getDay());
        System.out.println(date2.getYear() + " " + dateNew(date2.getMonth()) + " " + date2.getDay());
    }

    public static int dateNew(int dataMonth) {
        return dataMonth + 1;
    }
}
