package OOP;
/*
Напишите метод, который удаляет повторяющиеся элементы из целочисленного списка типа ArrayList, используя следующий заголовок:

public static void removeDuplicate(ArrayList<Integer> list)

Напишите тестовую программу, которая запрашивает у пользователя 10 целых чисел для списка,
отображает несовпадающие целые числа в порядке их ввода и отделяет их друг от друга только одним пробелом.

Пример выполнения программы:

Введите десять целых чисел: 34 5 3 5 6 4 33 2 2 4
Несовпадающие целые числа равны 34 5 3 6 4 33 2
 */

import java.util.Scanner;
import java.util.ArrayList;

public class Task29 {
    public static void main(String[] args) {
        System.out.print("Введите десять целых чисел: ");
        ArrayList<Integer> list = new ArrayList<>();
        Scanner input = new Scanner(System.in);
        for (int i = 0; i < 10; i++) {
            list.add(input.nextInt());
        }

        removeDuplicate(list);

        System.out.print("Несовпадающие целые числа равны ");
        for (int i = 0; i < list.size(); i++)
            System.out.print(list.get(i) + " ");
    }

    public static void removeDuplicate(ArrayList<Integer> list) {
        ArrayList<Integer> temp = new ArrayList<>();
        for (int i = 0; i < list.size(); i++)
            if (!temp.contains(list.get(i)))
                temp.add(list.get(i));

        list.clear();
        for (int i = 0; i < temp.size(); i++)
            list.add(temp.get(i));
    }
}