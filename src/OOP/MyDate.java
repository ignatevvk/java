package OOP;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class MyDate {
    private int year;
    private int month;
    private int day;

    //Безаргументный конструктор, который создает объект типа MyDate для текущей даты.
    MyDate(){
        setDate(new GregorianCalendar().getTimeInMillis());
    }

    // Конструктор, который создает объект типа MyDate с указанным временем в миллисекундах, прошедших с 00:00, 1 января 1970 г.
    MyDate(long elapsedTime) {
        setDate(elapsedTime);
    }
   // Конструктор, который создает объект типа MyDate с указанными годом, месяцем и днем.
    MyDate(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }
   //Три getter-метода для полей данных year, month и day, соответственно.
   public int getYear() {
       return year;
   }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    //Метод с именем setDate(long elapsedTime), который присваивает новую дату объекту, используя прошедшее время.
    public void setDate(long elapsedTime){
        GregorianCalendar date = new GregorianCalendar();
        date.setTimeInMillis(elapsedTime);
        // Найти год, месяц и день из даты. Вы пишете свой код, чтобы заменить устаревшие методы get()
        year = date.get(Calendar.YEAR);
        month = date.get(Calendar.MONTH);
        day = date.get(Calendar.DAY_OF_MONTH);
    }
}

