package OOP;
/*
Напишите клиент этого класса — программу, которая предлагает пользователю ввести двумерный массив и
отображает позицию наибольшего элемента в этом массиве.

Пример выполнения программы:

Введите количество строчек и столбцов массива:
3 4

Введите массив:
23.5 35 2 10
4.5 3 45 3.5
35 44 5.5 9.6

Наибольший элемент массива, равный 45.0, находится в позиции (1, 2)
 */

import java.util.Scanner;

public class Task07Location {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Введите количество строчек и столбцов массива: ");
        int numberOfRows = input.nextInt();
        int numberOfColumns = input.nextInt();

        System.out.println("Введите массив: ");
        double[][] a = new double[numberOfRows][numberOfColumns];
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                a[i][j] = input.nextDouble();
            }
        }

        Location location = Location.locateLargest(a);
        System.out.println("Наибольший элемент массива, равный " + location.maxValue + ", находится в позиции("
                + location.row + ", " + location.column + ")");
    }
}
