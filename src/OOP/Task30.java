package OOP;
/*
Напишите метод, который возвращает объединение двух целочисленных списков типа ArrayList, используя следующий заголовок:

public static ArrayList<Integer> union(ArrayList<Integer> list1, ArrayList<Integer> list2)

Например, объединение двух списков {2, 3, 1, 5} и {3, 4, 6} равно {2, 3, 1, 5, 3, 4, 6}.
Напишите тестовую программу, которая запрашивает у пользователя два списка, каждый с пятью целыми числами,
и отображает результат их объединения. Числа должны быть отделены друг от друга только одним пробелом.

Пример выполнения программы:

Введите пять целых чисел для списка1: 3 5 45 4 3
Введите пять целых чисел для списка2: 33 51 5 4 13
Объединенный список равен 3 5 45 4 3 33 51 5 4 13
 */

import java.util.Scanner;
import java.util.ArrayList;

public class Task30 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Введите пять целых чисел для списка1: ");
        ArrayList<Integer> list1 = new ArrayList<>();
        for (int i = 0; i < 5; i++)
            list1.add(input.nextInt());

        System.out.print("Введите пять целых чисел для списка2: ");
        ArrayList<Integer> list2 = new ArrayList<>();
        for (int i = 0; i < 5; i++)
            list2.add(input.nextInt());

        ArrayList<Integer> list3 = add(list1, list2);

        System.out.print("Объединенный список равен ");
        for (int i = 0; i < list3.size(); i++)
            System.out.print(list3.get(i) + " ");
    }

    public static ArrayList<Integer> add(
            ArrayList<Integer> list1, ArrayList<Integer> list2) {
        ArrayList<Integer> result = new ArrayList<>();

        for (int i = 0; i < list1.size(); i++)
            result.add(list1.get(i));

        for (int i = 0; i < list2.size(); i++)
            result.add(list2.get(i));

        return result;
    }
}
