package OOP;
/*
Напишите программу, которая создает список типа ArrayList и добавляет в него объект типа Loan,
объект типа Date, строку и объект типа Circle, а также используйте цикл для отображения всех элементов в списке путем
вызова метода toString() этих объектов.
 */
import java.util.*;

public class Task24 {
    public static void main(String[] args) {
        ArrayList<Object> list = new ArrayList<>();
        list.add(new Loan());
        list.add("ABC");
        list.add(new Date());

        for (int i = 0; i < list.size(); i++)
            System.out.println(list.get(i));
    }
}

