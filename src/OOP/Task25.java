package OOP;
/*
Напишите следующий метод, который перетасовывает элементы целочисленного списка типа ArrayList:

public static void shuffle(ArrayList<Integer> list)
 */

import java.util.ArrayList;

public class Task25 {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(i);
        }

        shuffle(list);

        for (int i = 0; i < list.size(); i++) {
            System.out.print(list.get(i) + " ");
        }
    }

    public static void shuffle(ArrayList<Integer> list) {
        for (int i = 0; i < list.size(); i++) {
            int index = (int)(Math.random() * list.size());
            int temp = list.get(index);
            list.set(index, list.get(i));
            list.set(i, temp);
        }
    }
}
