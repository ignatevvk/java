package OOP;

public class Task21 {
    public static void main(String[] args) {
        MyStringBuilder2 s1 = new MyStringBuilder2(new char[]{'a', 'b', 'c', 'd', 'p'});
        MyStringBuilder2 s2 = new MyStringBuilder2("xyz");

        System.out.println(s1.toString());
        System.out.println(s2.toString());

        System.out.println(s1.insert(3, s2).toString());
        System.out.println(s1.reverse().toString());
        System.out.println(s1.substring(2).toString());
        System.out.println(s1.toUpperCase().toString());
    }
}
