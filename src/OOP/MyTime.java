package OOP;

public class MyTime {

    private int hour;
    private int minute;
    private int second;

    //Безаргументный конструктор, который создает объект типа MyTime для текущего времени
    public MyTime() {
        this(System.currentTimeMillis());
    }

    // Конструктор, который создает объект типа MyTime с указанным временем в миллисекундах,
    // прошедших с 00:00, 1 января 1970 г. (Значения полей данных этого объекта будут представлять это время.)
    public MyTime(long elapsedTime) {
        setTime(elapsedTime);
    }

    //Конструктор, который создает объект типа MyTime с указанными часами, минутами и секундами.
    public MyTime(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    //getter-метод
    public int getHour() {
        return hour;
    }
    //getter-метод
    public int getMinute() {
        return minute;
    }
    //getter-метод
    public int getSecond() {
        return second;
    }
    // Метод с именем setTime(long elapseTime), который присваивает объекту новое время с помощью прошедшего времени.
    //Например, если прошедшее время равно 555550000 миллисекундам, то hour равно 10, minute равно 19, а second равно 10.
    public void setTime(long elapsedTime) {
        // Вычислить количество секунд, прошедших с 00:00, 1 января 1970 г.
        long totalSeconds = elapsedTime / 1000;

        // Вычислить текущую секунду в минуте и часе
        second = (int) (totalSeconds % 60);

        // Вычислить количество минут
        long totalMinutes = totalSeconds / 60;

        // Вычислить текущую минуту в часе
        minute = (int) (totalMinutes % 60);

        // Вычислить количество часов
        int totalHours = (int) (totalMinutes / 60);

        // Вычислить текущий час
        hour = (int) (totalHours % 24);
    }
}
