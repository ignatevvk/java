package OOP;
/*
Напишите метод, который возвращает из строки символьный массив типа ArrayList, используя следующий заголовок:

public static ArrayList<Character> toCharacterArray(String s)

Например, toCharacterArray("abc") возвращает список, содержащий символы 'a', 'b' и 'c'.
 */

import java.util.ArrayList;

public class Task31 {
    public static void main(String[] args) {
        for (Character ch: toCharacterArray("Welcome")) {
            System.out.println(ch);
        }
    }

    public static ArrayList<Character> toCharacterArray(String s) {
        ArrayList<Character> list = new ArrayList<>();
        for (int i = 0; i < s.length(); i++) {
            list.add(s.charAt(i));
        }
        return list;
    }
}
