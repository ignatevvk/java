package OOP;
/*
Создайте класс с именем StopWatch для представления секундомера. Класс StopWatch должен содержать:
o	Скрытые поля данных startTime и endTime с getter-методами.
o	Безаргументный конструктор, который инициализирует startTime с текущим временем.
o	Метод с именем start(), который сбрасывает startTime до текущего времени.
o	Метод с именем stop(), который присваивает endTime текущее время.
o	Метод с именем getElapsedTime(), который возвращает прошедшее время на секундомере в миллисекундах.

 */

public class StopWatch {
    private long startTime;
    private long endTime;
     java.util.Date date = new java.util.Date();

    StopWatch() {
        startTime = date.getTime();

    }

    public void start() {
        startTime = date.getTime();

    }

    public void stop() {
        endTime = date.getTime();

    }

    public long getElapsedTime() {
        return endTime - startTime;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTime;
    }
}
