package OOP;
/*
Напишите программу, которая создает объект типа Random с начальным (случайным) значением 1000
и отображает первые 50 случайных целых чисел между 0 и 100 с помощью метода nextInt(100).
 */

import java.util.Random;

public class Task04Random {
    public static void main(String[] args) {

        Random generator = new Random(1000);

        for (int i = 1; i <= 50; i++)
        System.out.print (generator.nextInt(101)+" ");
    }
}
