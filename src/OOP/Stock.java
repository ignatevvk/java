package OOP;

public class Stock {

    String symbol;
    String name;
    double previousClosingPrice;
    double currentPrice;

    //Конструктор, создающий акции с указанными обозначением и наименованием.
    Stock(String newSymbol, String newName) {
        symbol = newSymbol;
        name = newName;
    }

    /**
     * Метод с именем getChangePercent(),
     * который возвращает процент изменения стоимости акций с previousClosingPrice на currentPrice.
     */
    double getChangePercent(double previousClosingPrice, double currentPrice) {
        return ((currentPrice - previousClosingPrice) / currentPrice) * 100;
    }
}
