package OOP;
/*
Напишите следующий метод сортировки числового списка типа ArrayList:

public static void sort(ArrayList<Integer> list)

Напишите тестовую программу, которая запрашивает у пользователя пять чисел,
сохраняет их в списке и отображает в порядке возрастания.
 */

import java.util.ArrayList;
import java.util.Scanner;

public class Task27 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        ArrayList<Integer> list = new ArrayList<Integer>();
        System.out.print("Введите пять целых чисел: ");
        for (int i = 0; i < 5; i++)
            list.add(input.nextInt());

        sort(list);

        for (int i = 0; i < list.size(); i++)
            System.out.print(list.get(i) + " ");
    }

    public static void sort(ArrayList<Integer> list) {
        for (int i = 0; i < list.size() - 1; i++) {
            // Найти наименьший элемент в list[i..list.length-1]
            int currentMin = list.get(i);
            int currentMinIndex = i;

            for (int j = i + 1; j < list.size(); j++) {
                if (currentMin > list.get(j)) {
                    currentMin = list.get(j);
                    currentMinIndex = j;
                }
            }

            // Переставить list.get(i) и list.get(currentMinIndex) при необходимости
            if (currentMinIndex != i) {
                list.set(currentMinIndex, list.get(i));
                list.set(i, currentMin);
            }
        }
    }
}