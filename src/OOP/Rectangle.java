package OOP;

public class Rectangle {
    /**
     * Тип данных с значениями по умолчанию
     */
    double width = -1;
    double height = -1;

    /**
     * Безаргументный конструктор, создающий прямоугольник с указанными по умолчанию значениями
     */
    Rectangle() {
    }

    /**
     * Конструктор, создающий прямоугольник с указанными шириной и высотой
     */
    Rectangle(double newWidth, double newHeight) {
        width = newWidth;
        height = newHeight;
    }

    /**
     * Возвращает площадь этого прямоугольника
     */
    double getArea() {
        return width * height;
    }

    /**
     * Возвращает периметр этого прямоугольника
     */
    double getPerimeter() {
        return 2 * width + 2 * height;
    }
}
