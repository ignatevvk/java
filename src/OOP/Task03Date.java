package OOP;
/*
Напишите программу, которая создает объект Date, устанавливает у него
прошедшее время, равным 10000, 100000, 1000000, 10000000, 100000000, 1000000000,
10000000000 и 100000000000, и отображает дату и время с помощью метода toString(), соответственно.
 */

import java.util.Date;

public class Task03Date {

    public static void main(String[] args) {

        long t = 10000;

        for (int i = 1; i <= 8; i++) {
            java.util.Date date = new Date(t);
            System.out.println(date.toString());
            t = t * 10;
        }
    }
}
