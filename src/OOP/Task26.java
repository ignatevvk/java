package OOP;
/*
Напишите программу, которая случайным образом заполняет матрицу n x n значениями 0 и 1,
отображает эту матрицу в консоли и находит строчки и столбцы с наибольшим количеством 1.
(Подсказка: используйте два списка типа ArrayList для хранения индексов строчек и столбцов с наибольшим количеством 1.)

Далее приведен пример выполнения этой программы:

Введите размер матрицы: 4
Матрица со случайными значениями равна
0011
0011
1101
1010
Индекс строчки с наибольшим кол-вом единиц: 2
Индекс столбца с наибольшим кол-вом единиц: 2, 3
 */

import java.util.ArrayList;
import java.util.Scanner;

public class Task26 {
    public static void main(String[] args) {
        System.out.print("Введите размер матрицы: ");
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();

        System.out.println("Матрица со случайными значениями равна");
        int[][] matrix = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = (int)(Math.random() * 2);
                System.out.print(matrix[i][j]);
            }

            System.out.println();
        }

        // Проверить строчки
        int rowSum = sumRow(matrix[0]);
        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(0);
        for (int i = 1; i < n; i++) {
            if (rowSum < sumRow(matrix[i])) {
                rowSum = sumRow(matrix[i]);
                list.clear();
                list.add(i);
            }
            else if (rowSum == sumRow(matrix[i])) {
                list.add(i);
            }
        }

        System.out.print("Индекс строчки с наибольшим кол-вом единиц: ");
        for (int i = 0; i < list.size() - 1; i++)
            System.out.print(list.get(i) + ", ");
        System.out.print(list.get(list.size() - 1));

        // Проверить столбцы
        int columnSum = sumColumn(matrix, 0);
        list.clear();
        list.add(0);
        for (int i = 1; i < n; i++) {
            if (columnSum < sumColumn(matrix, i)) {
                columnSum = sumColumn(matrix, i);
                list.clear();
                list.add(i);
            }
            else if (columnSum == sumColumn(matrix, i)) {
                list.add(i);
            }
        }

        System.out.print("\nИндекс столбца с наибольшим кол-вом единиц: ");
        for (int i = 0; i < list.size() - 1; i++)
            System.out.print(list.get(i) + ", ");
        System.out.print(list.get(list.size() - 1));
    }

    public static int sumRow(int row[]) {
        int sum = 0;
        for (int i = 0; i < row.length; i++)
            sum += row[i];
        return sum;
    }

    public static int sumColumn(int matrix[][], int column) {
        int sum = 0;
        for (int i = 0; i < matrix.length; i++)
            sum += matrix[i][column];
        return sum;
    }
}