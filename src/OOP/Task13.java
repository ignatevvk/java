package OOP;
/*
Напишите программу, которая находит 5 простых чисел, которые больше Long.MAX_VALUE.
делятся на себя и на 1
 */

import java.math.BigInteger;

public class Task13 {

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();

        // bigNum представляет Long.MAX_VALUE
        BigInteger bigNum = new BigInteger(Long.MAX_VALUE + "");
        // теперь bigNum больше, чем Long.MAX_VALUE
        bigNum.add(BigInteger.ONE);

        int count = 1;
        while (count <= 5) {
            if (isPrime(bigNum)) {
                System.out.println(bigNum);
                count++;
            }
            bigNum = bigNum.add(BigInteger.ONE);
        }

        long endTime = System.currentTimeMillis();
        System.out.println("Потраченное время равно " +
                (endTime - startTime) + " мс");
    }

    public static boolean isPrime(BigInteger num) {
        if (num.compareTo(BigInteger.ONE) == 0 ||
                num.compareTo(new BigInteger("2")) == 0) {
            return true;
        }

        BigInteger halfNum = num.divide(new BigInteger("2"));

        for (BigInteger divisor = new BigInteger("2");
             divisor.compareTo(halfNum) <= 0;
             divisor = divisor.add(BigInteger.ONE)) {
            if (num.remainder(divisor).equals(BigInteger.ZERO)) {
                return false;
            }
        }
        return true;
    }
}

