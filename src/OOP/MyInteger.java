package OOP;

public class MyInteger {
    private int value;


    // Конструктор, который создает объект типа MyInteger для указанного значения типа int
    MyInteger(int value) {
        this.value = value;
    }

    // Getter-метод, который возвращает значение типа int.
    public int getValue() {
        return value;
    }


    //Методы isEven(), isOdd() и isPrime(), которые возвращают значение true,
    // если значение типа int в этом объекте является четным, нечетным или простым соответственно.

    public boolean isPrime() {
        return isPrime(value);
    }

    public static boolean isPrime(int num) {
        if ((num == 1) || (num == 2)) {
            return true;
        }

        for (int i = 2; i <= num / 2; i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }

    public boolean isOdd() {
        return isOdd(value);
    }

    public static boolean isOdd(int n) {
        return n % 2 != 0;
    }

    public boolean isEven() {
        return isEven(value);
    }

    public static boolean isEven(int n) {
        return n % 2 == 0;
    }


    //Методы equals(int) и equals(MyInteger), которые возвращают значение true,
    // если значение типа int в этом объекте равняется указанному значению.

    public boolean equals(int anotherNum) {
        return value == anotherNum;
    }

    public boolean equals(MyInteger o) {
        return value == o.getValue();
    }


    //Статический метод parseInt(char[]), который преобразует массив числовых символов в значение типа int.
    public static int parseInt(char[] numbers) {
        // numbers состоит из символов цифр.
        // Например, если numbers равно {'1', '2', '5'}, то возвращаемое значение
        // должно быть равным 125. Обратите внимание, что
        // numbers[0] равно '1'
        // numbers[1] равно '2'
        // numbers[2] равно '5'

        int result = 0;
        for (int i = 0; i < numbers.length; i++) {
            result = result * 10 + (numbers[i] - '0');
        }
        return result;
    }


    //Статический метод parseInt(String), который преобразует строку в значение типа int.
    public static int parseInt(String s) {
        // s состоит из символов цифр.
        // Например, если s равно "125", то возвращаемое значение
        // должно быть равным 125.
        int result = 0;
        for (int i = 0; i < s.length(); i++) {
            result = result * 10 + (s.charAt(i) - '0');
        }
        return result;
    }
}


