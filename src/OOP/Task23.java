package OOP;
/*
Напишите следующий метод, который возвращает наибольшее значение в целочисленном списке типа ArrayList.
Этот метод должен возвращать значение null, если список пустой или его размер равен 0.

public static Integer max(ArrayList<Integer> list)

Напишите тестовую программу, которая запрашивает у пользователя последовательность чисел, заканчивающуюся 0,
и вызывает этот метод для возврата наибольшего значения из входных данных.
 */

import java.util.ArrayList;
import java.util.Scanner;

public class Task23 {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();

        Scanner input = new Scanner(System.in);
        System.out.print("Введите целые числа, закончив ввод 0: ");
        int value;

        do {
            value = input.nextInt();

            if (value != 0)
                list.add(value);
        } while (value != 0);

        System.out.print("Наибольшее число равно " + max(list));
    }

    public static Integer max(ArrayList<Integer> list) {
        if (list == null || list.size() == 0)
            return null;

        int result = list.get(0);
        for (int i = 1; i < list.size(); i++)
            if (result < list.get(i))
                result = list.get(i);

        return result;
    }
}
