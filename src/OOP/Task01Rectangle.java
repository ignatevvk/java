package OOP;
/*
По примеру класса Circle, создайте класс Rectangle для представления прямоугольника. Класс Rectangle должен содержать:
o	Два поля данных типа double с именами width и height, задающими ширину и высоту прямоугольника. Значение по умолчанию:
-1 как для ширины, так и для высоты.
o	Безаргументный конструктор, создающий прямоугольник с указанными по умолчанию значениями.
o	Конструктор, создающий прямоугольник с указанными шириной и высотой.
o	Метод с именем getArea(), возвращающий площадь этого прямоугольника.
o	Метод с именем getPerimeter(), возвращающий периметр.
Нарисуйте (на бумаге или в графическом редакторе) UML-диаграмму класса Rectangle, а затем реализуйте этот класс.
Напишите клиент этого класса — программу, которая создает два объекта типа Rectangle: первый — с шириной 4 и высотой 40,
 а второй - с шириной 3.5 и высотой 35.9. Программа также должна отображать ширину, высоту,
 площадь и периметр каждого прямоугольника в указанном порядке.
 */

public class Task01Rectangle {
    public static void main(String[] args) {

        // Первый объект прямоугольник
        double width01 = 4;
        double height01 = 40;

        Rectangle rectangle01 = new Rectangle(width01, height01);
        System.out.println(width01);
        System.out.println(height01);
        System.out.println(rectangle01.getArea());
        System.out.println(rectangle01.getPerimeter());

        System.out.println();

        // Второй объект прямоугольник
        double width02 = 3.5;
        double height02 = 35.9;

        Rectangle rectangle02 = new Rectangle(width02, height02);
        System.out.println(width02);
        System.out.println(height02);
        System.out.println(rectangle02.getArea());
        System.out.println(rectangle02.getPerimeter());
    }
}
