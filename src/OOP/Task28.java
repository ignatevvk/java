package OOP;
/*
Напишите следующий метод, который возвращает сумму всех чисел в списке типа ArrayList:

public static double sum(ArrayList<Double> list)

Напишите тестовую программу, которая запрашивает у пользователя пять чисел, сохраняет их в списке и отображает их сумму
 */

import java.util.ArrayList;
import java.util.Scanner;

public class Task28 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        ArrayList<Double> list = new ArrayList<Double>();
        System.out.print("Введите пять целых чисел: ");
        for (int i = 0; i < 5; i++)
            list.add(input.nextDouble());

        System.out.println("Сумма равна " + sum(list));
    }

    public static double sum(ArrayList<Double> list) {
        double sum = 0;
        for (int i = 0; i < list.size(); i++)
            sum += list.get(i);
        return sum;
    }
}