package Dz03;
/*
Дано натуральное число n. Вывести его цифры в "столбик"
Ограничения
0< n< 1000000
Входные данные
74
Выходные данные
7
4
 */

import java.util.Scanner;

public class dz04_ {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String n = scanner.nextLine();
        for (int i = 0; i < n.length(); ++i) {
            System.out.println(n.charAt(i));
        }
    }
}
