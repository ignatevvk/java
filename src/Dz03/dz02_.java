package Dz03;
/* Сумма чисел.
На вход подается два положительных числа m и n. Найти сумму чисел между m и n включительно.
Ограничение: 0 < m, n < 10, m < n

Пример входных данных
7 9
Пример выходных
24
 */

import java.util.Scanner;

public class dz02_ {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int sum = 0;
        while (m <= n) {
            sum = sum + m;
            ++m;
        }
        System.out.println(sum);
    }
}
