package Dz03;
/*
Напечатать таблицу умножения от 1 до 9.
1 x 1 = 1
1 x 2 = 2
.
.
9 x 8 = 72
9 x 9 = 81
 */

public class dz01_ {
    public static void main(String[] args) {
        int a;
        int b;
        for (a = 1; a <= 9; ++a) {
            for (b = 1; b <= 9; ++b) {
                System.out.println(a + " x " + b + " = " + (a * b));
            }
        }
    }
}
