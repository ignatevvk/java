package Dz03;
/*
Дана строка s.
Вычислить кол-во символлов в ней, не считая пробелов (необходимо использовать цикл).
 */

import java.util.Scanner;

public class dz07_ {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        int sumSymbol = 0;
        char symbol = ' ';

        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == symbol) {
                sumSymbol++;
            }
        }
        System.out.println(s.length() - sumSymbol);
    }
}
