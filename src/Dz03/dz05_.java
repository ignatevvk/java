package Dz03;
/*
Даны положительные натуральные числа m и n
Найти остаток от деления m на n, не выполняя операцию взятие остатка.

Ограничения
0 < m;
n < 10;

Входные данные
9 1
Выходные данные
0
 */

import java.util.Scanner;

public class dz05_ {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int ost = m;

       //System.out.println("Проверка: " + m % n);

        if (m == n) {
            System.out.println("0");
        } else if (m < n) {
            System.out.println(m);
        } else {
            while (ost >= n) {
                ost = ost - n;
            }
            System.out.println(ost);
        }
    }
}





