package Dz03;
/*
Вывести на экран "ёлочку" из символа решетки (#) заданной высоты N.
На N + 1 строке у "ёлочки" должен быть отображен ствол из символа |

Ограничение:
2 < n < 10

      #
     ###
    #####
      |
 */

import java.util.Scanner;

public class dz10_ {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        for (int i = 0; i < n; i++) {

            if (i == 0) {
                for (int k = 0; k < n - 1; k++) {
                    System.out.print(" ");
                }
                System.out.println("#");

            } else {
                for (int b = 0; b < n - i - 1; b++) {
                    System.out.print(" ");
                }

                for (int j = 0; j < 2 * i + 1; j++) {
                    System.out.print("#");
                }
                System.out.println();
            }
        }
        for (int k = 0; k < n - 1; k++) {
            System.out.print(" ");
        }
        System.out.print("|");
    }
}

