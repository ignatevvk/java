package Dz03;
/*
На вход подается два положительных числа m и n
необходимо вычислить m^1+ m^2+...+m^n
Ограничение:
0<m, n<10

Входные данные
1 1
Выходные данные
1
 */

import java.util.Scanner;

public class dz03_ {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double m = scanner.nextDouble();
        int n = scanner.nextInt();
        int sum = 0;

        for (int i = 1; i <= n; ++i) {
            sum = (int) (sum + Math.pow(m, (double) i));
        }
        System.out.println(sum);
    }
}
