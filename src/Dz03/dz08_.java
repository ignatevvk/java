package Dz03;
/*
На вход подаётся:
- целое число n
- целое число p
- целое число a1,a2,....an

Необходимо вычислить сумму всех чисел a1,a2,....an
которые строго больше p

 */

import java.util.Scanner;

public class dz08_ {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int p = scanner.nextInt();
        int sum = 0;

        for (int i = 1; i <= n; i++) {
            int a = scanner.nextInt();
            if (a > p) {
                sum = sum + a;
            }
        }
        System.out.println(sum);
    }
}
