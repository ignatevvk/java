package abstractClass.task17;
/*
Задание №17

Перепишите класс Rational, используя новое внутреннее представление числителя и знаменателя.
Создайте массив из двух целых чисел следующим образом:

private long[] r = new long[2];

Используйте r[0] для представления числителя, а r[1] для представления знаменателя.
Сигнатуры методов в классе Rational не изменяются, поэтому клиентское приложение,
использующее предыдущий класс Rational, может продолжать использовать новый класс Rational без повторной компиляции.
 */

class NewRational extends Number implements Comparable<NewRational> {
    // Поля данных для числителя и знаменателя
    private long[] r = new long[2];

    /** Заданный по умолчанию конструктор */
    public NewRational() {
        this(0, 1);
    }

    /** Создает радиональное число с указанным числителем и знаменателем */
    public NewRational(long numerator, long denominator) {
        long gcd = gcd(numerator, denominator);
        this.r[0] = numerator/gcd;
        this.r[1] = denominator/gcd;
    }

    /** Находит НОД двух чисел */
    private long gcd(long n, long d) {
        long t1 = Math.abs(n);
        long t2 = Math.abs(d);
        long remainder = t1 % t2;

        while (remainder != 0) {
            t1 = t2;
            t2 = remainder;
            remainder = t1 % t2;
        }

        return t2;
    }

    /** Возвращает числитель */
    public long getNumerator() {
        return r[0];
    }

    /** Возвращает знаменатель */
    public long getDenominator() {
        return r[1];
    }

    /** Прибавляет рациональное число к текущему */
    public NewRational add(NewRational secondNewRational) {
        long n = r[0] * secondNewRational.getDenominator() +
                r[1] * secondNewRational.getNumerator();
        long d = r[1] * secondNewRational.getDenominator();
        return new NewRational(n, d);
    }

    /** Вычитает рациональное число из текущего */
    public NewRational subtract(NewRational secondNewRational) {
        long n = r[0]*secondNewRational.getDenominator()
                - r[1]*secondNewRational.getNumerator();
        long d = r[1]*secondNewRational.getDenominator();
        return new NewRational(n, d);
    }

    /** Умножает рациональное число на текущее */
    public NewRational multiply(NewRational secondNewRational) {
        long n = r[0] * secondNewRational.getNumerator();
        long d = r[1] * secondNewRational.getDenominator();
        return new NewRational(n, d);
    }

    /** Делит на рациональное число текущее */
    public NewRational divide(NewRational secondNewRational) {
        long n = r[0] * secondNewRational.getDenominator();
        long d = r[1] * secondNewRational.r[0];
        return new NewRational(n, d);
    }

    @Override // Переопределяет метод toString класса Object
    public String toString() {
        if (r[1] == 1)
            return r[0] + "";
        else
            return r[0] + "/" + r[1];
    }

    @Override // Переопределяет метод equals класса Object
    public boolean equals(Object parm1) {
        if ((this.subtract((NewRational)(parm1))).getNumerator() == 0)
            return true;
        else
            return false;
    }

    @Override // Реализует абстрактный метод intValue класса Number
    public int intValue() {
        return (int)doubleValue();
    }

    @Override // Реализует абстрактный метод floatValue класса Number
    public float floatValue() {
        return (float)doubleValue();
    }

    @Override // Реализует абстрактный метод doubleValue класса Number
    public double doubleValue() {
        return r[0] * 1.0 / r[1];
    }

    @Override // Реализует абстрактный метод longValue класса Number
    public long longValue() {
        return (long)doubleValue();
    }

    @Override // Реализует метод compareTo интерфейса Comparable
    public int compareTo(NewRational o) {
        if ((this.subtract((NewRational)o)).getNumerator() > 0)
            return 1;
        else if ((this.subtract((NewRational)o)).getNumerator() < 0)
            return -1;
        else
            return 0;
    }
}
