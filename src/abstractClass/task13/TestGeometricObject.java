package abstractClass.task13;
/*

Задание №13

Перепишите класс Circle в программе TestGeometricObject для наследования от класса GeometricObject и
реализации интерфейса Comparable. Переопределите метод equals() в классе Object.
Два объекта типа Circle равны, если их радиусы одинаковы. Нарисуйте UML-диаграмму,
включающую Circle, GeometricObject и Comparable.
 */

import OOP.GeometricObject;

public class TestGeometricObject {
    public static void main(String[] args) {
        Circle obj1 = new Circle();
        Circle obj2 = new Circle();

        System.out.println(obj1.equals(obj2));
        System.out.println(obj1.compareTo(obj2));
    }
}

class Circle extends GeometricObject implements Comparable<Circle> {
    private double radius;

    /** Возвращает радиус */
    public double getRadius() {
        return radius;
    }

    /** Присваивает новый радиус */
    public void setRadius(double radius) {
        this.radius = radius;
    }

    /** Реализует метод getArea, определенный в классе GeometricObject */
    public double getArea() {
        return radius * radius * Math.PI;
    }

    /** Реализует метод getPerimeter, определенный в классе GeometricObject*/
    public double getPerimeter() {
        return 2 * radius * Math.PI;
    }

    @Override
    public String toString() {
        return "[круг] радиус = " + radius;
    }

    @Override
    public int compareTo(Circle obj) {
        if (this.getArea() > obj.getArea())
            return 1;
        else if (this.getArea() < obj.getArea())
            return -1;
        else
            return 0;
    }

    public boolean equals(Object obj) {
        return this.radius == ((Circle)obj).radius;
    }
}
