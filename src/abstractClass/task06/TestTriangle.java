package abstractClass.task06;
/*
Задание №6
Спроектируйте новый класс Triangle, который наследуется от абстрактного класса GeometricObject.
 Нарисуйте UML-диаграмму классов Triangle и GeometricObject, а затем реализуйте класс Triangle.
  Напишите тестовую программу, которая запрашивает у пользователя ввод трёх сторон треугольника,
  цвета и логического значения для указания заливки. Программа должна создать объект типа Triangle с
   этими сторонами и задать свойства color и filled, используя введенные пользователем данные.
   Программа должна отображать площадь, периметр, цвет и значение true или false для указания заливки.

 */

import OOP.GeometricObject;

import java.util.Scanner;

public class TestTriangle {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Введите три стороны: ");
        double side1 = input.nextDouble();
        double side2 = input.nextDouble();
        double side3 = input.nextDouble();

        Triangle triangle = new Triangle(side1, side2, side3);

        System.out.print("Введите цвет: ");
        String color = input.next();
        triangle.setColor(color);

        System.out.print("Введите true/false для заливки: ");
        boolean filled = input.nextBoolean();
        triangle.setFilled(filled);

        System.out.println("Площадь равна " + triangle.getArea());
        System.out.println("Периметр равен "
                + triangle.getPerimeter());
        System.out.println(triangle);
    }
}

class Triangle extends GeometricObject {
    private double side1 = 1.0, side2 = 1.0, side3 = 1.0;

    public Triangle() {
    }

    public Triangle(double side1, double side2, double side3) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    public double getArea() {
        double s = (side1 + side2 + side3) / 2;
        return Math.sqrt(s * (s - side1) * (s - side2) * (s - side3));
    }

    public double getPerimeter() {
        return side1 + side2 + side3;
    }

    @Override
    public String toString() {
        return "Треугольник: сторона 1 = " + side1 + " сторона 2 = " + side2 +
                " сторона 3 = " + side3;
    }
}