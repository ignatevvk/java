package abstractClass.task10;
/*

Задание №10

Определите класс с именем ComparableCircle, который наследуется от Circle и реализует Comparable.
Нарисуйте UML-диаграмму и реализуйте метод compareTo() для сравнения кругов на основе их площади.
Напишите тестовую программу, чтобы найти наибольший из двух экземпляров класса ComparableCircle и
наибольший между кругом и прямоугольником.
 */

public class TestComparableCircle {
    public static void main(String[] args) {
        // Создать два сравнимых круга
        ComparableCircle circle1 = new ComparableCircle(5);
        ComparableCircle circle2 = new ComparableCircle(15);

        // Отобразить наибольший круг
        ComparableCircle circle3 = (ComparableCircle)Max.max(circle1, circle2);
        System.out.println("Радиус наибольшего круга равен " + circle3.getRadius());
        System.out.println(circle3);
    }
}

class ComparableCircle extends Circle implements Comparable<ComparableCircle> {
    /** Создает сравнимый круг с указанным радиусом */
    public ComparableCircle(double radius) {
        super(radius);
    }

    @Override
    public int compareTo(ComparableCircle o) {
        if (getRadius() > o.getRadius())
            return 1;
        else if (getRadius() < o.getRadius())
            return -1;
        else
            return 0;
    }
}

class Max {
    /** Возвращает наибольшую из двух фигур */
    public static ComparableCircle max
    (ComparableCircle o1, ComparableCircle o2) {
        if (o1.compareTo(o2) > 0)
            return o1;
        else
            return o2;
    }
}
