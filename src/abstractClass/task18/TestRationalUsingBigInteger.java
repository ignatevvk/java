package abstractClass.task18;
/*
Перепроектируйте и реализуйте класс Rational, используя BigInteger для числителя и знаменателя.
Напишите тестовую программу, которая запрашивает у пользователя ввести два рациональных числа и отображает результаты,
 как показано в следующем примере:

Введите первое рациональное число: 3 454
Введите второе рациональное число: 7 2389
3/454 + 7/2389 = 10345/1084606
3/454 - 7/2389 = 3989/1084606
3/454 * 7/2389 = 21/1084606
3/454 / 7/2389 = 7167/3178
7/2389 равно 0.0029300962745918793
 */

import java.math.*;
import java.util.Scanner;

public class TestRationalUsingBigInteger {
    public static void main(String[] args) {
        // Получить два рациональных числа
        Scanner input = new Scanner(System.in);
        System.out.print("Введите числитель и знаменатель первого рационального числа через пробел: ");
        String n1 = input.next();
        String d1 = input.next();

        System.out.print("Введите числитель и знаменатель второго рационального числа через пробел: ");
        String n2 = input.next();
        String d2 = input.next();

        RationalUsingBigInteger r1 = new RationalUsingBigInteger(new BigInteger(n1), new BigInteger(d1));
        RationalUsingBigInteger r2 = new RationalUsingBigInteger(new BigInteger(n2), new BigInteger(d2));

        // Отобразить результаты
        System.out.println(r1 + " + " + r2 + " = " + r1.add(r2));
        System.out.println(r1 + " - " + r2 + " = " + r1.subtract(r2));
        System.out.println(r1 + " * " + r2 + " = " + r1.multiply(r2));
        System.out.println(r1 + " / " + r2 + " = " + r1.divide(r2));
        System.out.println(r2 + " равно " + r2.doubleValue());
    }
}

class RationalUsingBigInteger extends Number implements Comparable<RationalUsingBigInteger> {
    // Поля данных для числителя и знаменателя
    private BigInteger numerator = BigInteger.ZERO;
    private BigInteger denominator = BigInteger.ONE;

    /** Создает рациональное число с заданными по умолчанию свойствами */
    public RationalUsingBigInteger() {
        this(BigInteger.ZERO, BigInteger.ONE);
    }

    /** Создает рациональное число с указанным числителем и знаменателем */
    public RationalUsingBigInteger(BigInteger numerator, BigInteger denominator) {
        BigInteger gcd = gcd(numerator, denominator);

        if (denominator.compareTo(BigInteger.ZERO) < 0)
            this.numerator = numerator.multiply(new BigInteger("-1")).divide(gcd);
        else
            this.numerator = numerator.divide(gcd);
        this.denominator = denominator.abs().divide(gcd);
    }

    /** Находит НОД двух чисел */
    private static BigInteger gcd(BigInteger n, BigInteger d) {
        BigInteger n1 = n.abs();
        BigInteger n2 = d.abs();
        BigInteger gcd = BigInteger.ONE;

        for (BigInteger k = BigInteger.ONE;
             k.compareTo(n1) <= 0 && k.compareTo(n2) <= 0;
             k = k.add(BigInteger.ONE)) {
            if (n1.remainder(k).equals(BigInteger.ZERO) &&
                    n2.remainder(k).equals(BigInteger.ZERO))
                gcd = k;
        }

        return gcd;
    }

    /** Возвращает числитель */
    public BigInteger getNumerator() {
        return numerator;
    }

    /** Возвращает знаменатель */
    public BigInteger getDenominator() {
        return denominator;
    }

    /** Прибавляет рациональное число к текущему */
    public RationalUsingBigInteger add(RationalUsingBigInteger secondRationalUsingBigInteger) {
        BigInteger n = numerator.multiply(secondRationalUsingBigInteger.getDenominator()).add(
                denominator.multiply(secondRationalUsingBigInteger.getNumerator()));
        BigInteger d = denominator.multiply(secondRationalUsingBigInteger.getDenominator());
        return new RationalUsingBigInteger(n, d);
    }

    /** Вычитает рациональное число из текущего */
    public RationalUsingBigInteger subtract(RationalUsingBigInteger secondRationalUsingBigInteger) {
        BigInteger n = numerator.multiply(secondRationalUsingBigInteger.getDenominator()).subtract(
                denominator.multiply(secondRationalUsingBigInteger.getNumerator()));
        BigInteger d = denominator.multiply(secondRationalUsingBigInteger.getDenominator());
        return new RationalUsingBigInteger(n, d);
    }

    /** Умножает рациональное число на текущее */
    public RationalUsingBigInteger multiply(RationalUsingBigInteger secondRationalUsingBigInteger) {
        BigInteger n = numerator.multiply(secondRationalUsingBigInteger.getNumerator());
        BigInteger d = denominator.multiply(secondRationalUsingBigInteger.getDenominator());
        return new RationalUsingBigInteger(n, d);
    }

    /** Делит на рациональное число текущее */
    public RationalUsingBigInteger divide(RationalUsingBigInteger secondRationalUsingBigInteger) {
        BigInteger n = numerator.multiply(secondRationalUsingBigInteger.getDenominator());
        BigInteger d = denominator.multiply(secondRationalUsingBigInteger.numerator);
        return new RationalUsingBigInteger(n, d);
    }

    @Override // Переопределяет метод toString класса Object
    public String toString() {
        if (denominator.equals(BigInteger.ONE))
            return numerator + "";
        else
            return numerator + "/" + denominator;
    }

    @Override // Переопределяет метод equals класса Object
    public boolean equals(Object parm1) {
        if ((this.subtract((RationalUsingBigInteger)(parm1))).getNumerator().equals(BigInteger.ONE))
            return true;
        else
            return false;
    }

    @Override // Переопределяет метод hashCode класса Object
    public int hashCode() {
        return new Double(this.doubleValue()).hashCode();
    }

    @Override // Реализует абстрактный метод intValue класса Number
    public int intValue() {
        return (int)doubleValue();
    }

    @Override // Реализует абстрактный метод floatValue класса Number
    public float floatValue() {
        return (float)doubleValue();
    }

    @Override // Реализует абстрактный метод doubleValue класса Number
    public double doubleValue() {
        return numerator.doubleValue() / denominator.doubleValue();
    }

    @Override // Реализует абстрактный метод longValue класса Number
    public long longValue() {
        return (long)doubleValue();
    }

    @Override // Реализует метод compareTo интерфейса Comparable
    public int compareTo(RationalUsingBigInteger o) {
        if ((this.subtract((RationalUsingBigInteger)o)).getNumerator().compareTo(BigInteger.ZERO) > 0)
            return 1;
        else if ((this.subtract((RationalUsingBigInteger)o)).getNumerator().compareTo(BigInteger.ZERO) < 0)
            return -1;
        else
            return 0;
    }
}