package abstractClass.task16;
/*

Задание №16

Перепишите класс Course из программы TestCourse в разделе «Объектно-ориентированное мышление»,
чтобы добавить метод clone() для выполнения глубокой копии поля students.
 */

public class TestCourse1 {
    public static void main(String[] args) {
        Course1 course1 = new Course1("ООП");
        course1.addStudent("S1");
        course1.addStudent("S2");
        course1.addStudent("S3");

        Course1 course2 = (Course1) course1.clone();
        course2.addStudent("S4");
        course2.addStudent("S5");
        course2.addStudent("S6");

        System.out.println("Студенты на course1: ");
        for (int i = 0; i < course1.getNumberOfStudents(); i++)
            System.out.print(course1.getStudents()[i] + " ");
        System.out.println("\nСтуденты на course1: ");
        for (int i = 0; i < course2.getNumberOfStudents(); i++)
            System.out.print(course2.getStudents()[i] + " ");
    }
}

class Course1 implements Cloneable {
    private String courseName;
    private String[] students = new String[100];
    private int numberOfStudents;

    public Course1(String courseName) {
        this.courseName = courseName;
    }

    public void addStudent(String student) {
        students[numberOfStudents] = student;
        numberOfStudents++;
    }

    public String[] getStudents() {
        return students;
    }

    public int getNumberOfStudents() {
        return numberOfStudents;
    }

    public String getCourse1Name() {
        return courseName;
    }

    public void dropStudent(String student) {
        // Это место оставлено для выполнения задания
    }

    public Object clone() {
        try {
            Course1 c = (Course1) super.clone();
            c.students = new String[100];
            System.arraycopy(students, 0, c.students, 0, 100);
            // Содержимое массива students - строки. Строки являются неизменяемыми,
            // поэтому здесь можно использовать метод arraycopy.
            // Если бы students представлял собой массив изменяемых объектов,
            // то необходимо было выполнять глубокие-глубокие копии.
            c.numberOfStudents = numberOfStudents;
            return c;
        } catch (CloneNotSupportedException ex) {
            return null;
        }
    }
}
