package abstractClass.task12;




import java.util.ArrayList;

class MyStack1 implements Cloneable {
    private ArrayList<Object> list = new ArrayList<>();

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public int getSize() {
        return list.size();
    }

    public Object peek() {
        return list.get(getSize() - 1);
    }

    public Object pop() {
        Object o = list.get(getSize() - 1);
        list.remove(getSize() - 1);
        return o;
    }

    public void push(Object o) {
        list.add(o);
    }

    /** Переопределяет метод toString класса Object */
    public String toString() {
        return "stack: " + list.toString();
    }

    public Object clone() {
        try {
            MyStack1 c = (MyStack1) super.clone();
            c.list = (ArrayList<Object>) this.list.clone();
            return c;
        } catch (CloneNotSupportedException ex) {
            return null;
        }
    }
}
