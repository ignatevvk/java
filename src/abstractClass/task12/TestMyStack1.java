package abstractClass.task12;
/*
Задание №12

Перепишите класс MyStack из раздела «Наследование и полиморфизм» для выполнения глубокой копии поля списка.
 */


public class TestMyStack1 {
    public static void main(String[] args) {
        MyStack1 stack = new MyStack1();
        stack.push("S1");
        stack.push("S2");
        stack.push("S");

        MyStack1 stack2 = (MyStack1) (stack.clone());
        stack2.push("S1");
        stack2.push("S2");
        stack2.push("S");

        System.out.println(stack.getSize());
        System.out.println(stack2.getSize());
    }
}
