package abstractClass.task14;
/*

Задание №14

Перепишите класс Rectangle в программе TestGeometricObject для наследования от класса GeometricObject
 и реализации интерфейса Comparable. Переопределите метод equals() в классе Object.
 Два объекта типа Rectangle равны, если их площади одинаковы. Нарисуйте UML-диаграмму,
 которая включает Rectangle, GeometricObject и Comparable.
 */

import OOP.GeometricObject;

public class TestGeometricObject {
    public static void main(String[] args) {
        Rectangle obj1 = new Rectangle();
        Rectangle obj2 = new Rectangle();
        System.out.println(obj1.equals(obj2));
        System.out.println(obj1.compareTo(obj2));
    }
}

class Rectangle extends GeometricObject implements Comparable<Rectangle> {
    private double width;
    private double height;

    /** Заданный по умолчанию конструктор */
    public Rectangle() {
        this(1.0, 1.0);
    }

    /** Создает прямоугольник с указанной шириной и высотой */
    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }

    /** Возвращает ширину */
    public double getWidth() {
        return width;
    }

    /** Присваивает новую ширину */
    public void setWidth(double width) {
        this.width = width;
    }

    /** Возвращает высоту */
    public double getHeight() {
        return height;
    }

    /** Присваивает новую высоту */
    public void setHeight(double height) {
        this.height = height;
    }

    /** Реализует метод getArea класса GeometricObject */
    public double getArea() {
        return width*height;
    }

    /** Реализует метод getPerimeter класса GeometricObject */
    public double getPerimeter() {
        return 2 * (width + height);
    }

    @Override
    public String toString() {
        return "[прямоугольник] ширина = " + width +
                " и высота = " + height;
    }

    @Override
    public int compareTo(Rectangle obj) {
        if (this.getArea() > obj.getArea())
            return 1;
        else if (this.getArea() < obj.getArea())
            return -1;
        else
            return 0;
    }

    public boolean equals(Object obj) {
        return this.getArea() == ((Rectangle)obj).getArea();
    }
}
