package abstractClass.task09;
/*

Задание №9

Измените класс GeometricObject, чтобы реализовать интерфейс Comparable и определить статический метод max()
в классе GeometricObject для поиска наибольшего из двух объектов типа GeometricObject.
Нарисуйте UML-диаграмму и реализуйте новый класс GeometricObject. Напишите тестовую программу,
которая использует метод max() для поиска наибольшего из двух кругов и наибольшего из двух прямоугольников.
 */

public class TestComparable {
    public static void main(String[] args) {
        // Создать два сравнимых круга
        Circle1 circle1 = new Circle1(5);
        Circle1 circle2 = new Circle1(4);

        // Отобразить наибольший круг
        Circle1 circle = (Circle1) GeometricObject1.max(circle1, circle2);
        System.out.println("Радиус наибольшего круга равен " + circle.getRadius());
        System.out.println(circle);

        // Создать два сравнимых прямоугольника
        Rectangle2 r1 = new Rectangle2(5, 4);
        Rectangle2 r2 = new Rectangle2(4, 5);

        System.out.println(r1.compareTo(r2));
        System.out.println("Наибольший прямоугольник равен " + GeometricObject1.max(r1, r2));

        System.out.println("Наибольшая геометрическая фигура равна " + GeometricObject1.max(circle1, r2));
    }
}

abstract class GeometricObject1 implements Comparable<GeometricObject1> {
    protected String color;
    protected double weight;

    /** Заданный по умолчанию конструктор */
    protected GeometricObject1() {
        color = "белый";
        weight = 1.0;
    }

    /** Создает геометрическую фигуру */
    protected GeometricObject1(String color, double weight) {
        this.color = color;
        this.weight = weight;
    }

    /** Getter-метод для цвета */
    public String getColor() {
        return color;
    }

    /** Setter-метод для цвета */
    public void setColor(String color) {
        this.color = color;
    }

    /** Getter-метод для веса */
    public double getWeight() {
        return weight;
    }

    /** Setter-метод для веса */
    public void setWeight(double weight) {
        this.weight = weight;
    }

    /** Абстрактный метод */
    public abstract double getArea();

    /** Абстрактный метод */
    public abstract double getPerimeter();

    public int compareTo(GeometricObject1 o) {
        if (this.getArea() < o.getArea())
            return -1;
        else if (getArea() == o.getArea())
            return 0;
        else
            return 1;

        // return getArea() < o.getArea() ? -1 : (getArea() == o.getArea() ? 0 : 1);
    }

    public static GeometricObject1 max(GeometricObject1 o1, GeometricObject1 o2) {
        if (o1.compareTo(o2) > 0)
            return o1;
        else
            return o2;

        // return o1.compareTo(o2) ? o1 : o2;
    }
}

class Circle1 extends GeometricObject1 {
    protected double radius;

    /** Заданный по умолчанию конструктор */
    public Circle1() {
        this(1.0, "белый", 1.0);
    }

    /** Создает круг с указанным радиусом */
    public Circle1(double radius) {
        super("белый", 1.0);
        this.radius = radius;
    }

    /** Создает круг с указанным радиусом, весом и цветом */
    public Circle1(double radius, String color, double weight) {
        super(color, weight);
        this.radius = radius;
    }

    /** Getter-метод для радиуса */
    public double getRadius() {
        return radius;
    }

    /** Setter-метод для радиуса */
    public void setRadius(double radius) {
        this.radius = radius;
    }

    /** Реализует абстрактный метод getArea класса GeometricObject */
    public double getArea() {
        return radius * radius * Math.PI;
    }

    /** Реализует абстрактный метод getPerimeter класса GeometricObject */
    public double getPerimeter() {
        return 2 * radius * Math.PI;
    }

    /** Переопределяет метод equals, определенный в классе Object */
    @Override
    public boolean equals(Object circle) {
        return this.radius == ((Circle1)circle).getRadius();
    }

    @Override
    public String toString() {
        return "[круг] радиус = " + radius;
    }

//  @Override
//  public int compareTo(GeometricObject1 o) {
//    if (getRadius() > ((Circle1) o).getRadius())
//      return 1;
//    else if (getRadius() < ((Circle1) o).getRadius())
//      return -1;
//    else
//      return 0;
//  }
}

class Rectangle2 extends GeometricObject1 {
    private double width;
    private double height;

    public Rectangle2() {
    }

    public Rectangle2(double width, double height) {
        this.width = width;
        this.height = height;
    }

    /** Возвращает ширину */
    public double getWidth() {
        return width;
    }

    /** Присваивает новую ширину */
    public void setWidth(double width) {
        this.width = width;
    }

    /** Возвращает высоту */
    public double getHeight() {
        return height;
    }

    /** Присваивает новую высоту */
    public void setHeight(double height) {
        this.height = height;
    }

    @Override /** Возвращает площадь */
    public double getArea() {
        return width * height;
    }

    @Override /** Возвращает периметр */
    public double getPerimeter() {
        return 2 * (width + height);
    }

    @Override
    public String toString() {
        return "[прямоугольник] ширина: " + width + " высота: " + height;
    }
}
