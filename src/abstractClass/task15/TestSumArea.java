package abstractClass.task15;
/*

Задание №15

Напишите метод, который суммирует площадь всех геометрических объектов в массиве. Сигнатура этого метода следующая:

public static double sumArea(GeometricObject[] a)

Напишите тестовую программу, которая создает массив из четырех объектов (два круга и два прямоугольника)
и вычисляет их общую площадь с помощью метода sumArea().
 */

import OOP.GeometricObject;

public class TestSumArea {
    public static void main(String[] args) {
        new TestSumArea();
    }

    public TestSumArea() {
        GeometricObject[] a = {new Circle(5), new Circle(6),
                new Rectangle(2, 3), new Rectangle(2, 3)};

        System.out.println("Общая площадь равна " + sumArea(a));
    }

    public static double sumArea(GeometricObject[] a) {
        double sum = 0;

        for (int i = 0; i < a.length; i++)
            sum += a[i].getArea();

        return sum;
    }
}

class Rectangle extends GeometricObject {
    private double width;
    private double height;

    /** Создает прямоугольник с указанной шириной и высотой */
    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }

    /** Возвращает ширину */
    public double getWidth() {
        return width;
    }

    /** Присваивает новую ширину */
    public void setWidth(double width) {
        this.width = width;
    }

    /** Возвращает высоту */
    public double getHeight() {
        return height;
    }

    /** Присваивает новую высоту */
    public void setHeight(double height) {
        this.height = height;
    }

    /** Реализует метод getArea класса GeometricObject */
    public double getArea() {
        return width * height;
    }

    /** Реализует метод getPerimeter класса GeometricObject*/
    public double getPerimeter() {
        return 2 * (width + height);
    }

    /** Переопределяет метод equals класса Object */
    public boolean equals(Rectangle rectangle) {
        return (width == rectangle.getWidth()) &&
                (height == rectangle.getHeight());
    }

    @Override
    public String toString() {
        return "[прямоугольник] ширина = " + width +
                " и высота = " + height;
    }
}
