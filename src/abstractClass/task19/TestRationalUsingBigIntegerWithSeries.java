package abstractClass.task19;
/*

Задание №19

Напишите программу, которая вычисляет следующий ряд с помощью класса Rational:

Вы обнаружите, что вывод неверен из-за целочисленного переполнения (слишком большое целое число).
Для решения этой проблемы см. предыдущее задание.
 */

import java.math.*;

public class TestRationalUsingBigIntegerWithSeries {
    public static void main(String[] args) {
        final int N = 100;
        RationalUsingBigInteger sum = new RationalUsingBigInteger();
        for (int i = 1; i <= N; i = i + 2) {
            sum = sum.add(new RationalUsingBigInteger(BigInteger.valueOf(i), BigInteger.valueOf(i + 1)));
        }
        System.out.println("Сумма ряда равна " + sum + " = " +
                sum.doubleValue());
    }
}

class RationalUsingBigInteger extends Number {
    // Поля данных для числителя и знаменателя
    private final BigInteger numerator;
    private final BigInteger denominator;

    /** Создает рациональное число с заданными по умолчанию свойствами */
    public RationalUsingBigInteger() {
        this(BigInteger.ZERO, BigInteger.ONE);
    }

    /** Создает рациональное число с указанным числителем и знаменателем */
    public RationalUsingBigInteger(BigInteger numerator, BigInteger denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    /** Возвращает числитель */
    public BigInteger getNumerator() {
        return numerator;
    }

    /** Возвращает знаменатель */
    public BigInteger getDenominator() {
        return denominator;
    }

    /** Прибавляет рациональное число к текущему */
    public RationalUsingBigInteger add(RationalUsingBigInteger second) {
        BigInteger n = numerator.multiply(second.getDenominator()).add(denominator.multiply(second.getNumerator()));
        BigInteger d = denominator.multiply(second.getDenominator());
        return new RationalUsingBigInteger(n, d);
    }

    // Переопределяет метод toString класса Object
    @Override
    public String toString() {
        if (denominator.equals(BigInteger.ONE))
            return numerator + "";
        else
            return numerator + "/" + denominator;
    }

    // Реализует абстрактный метод intValue класса Number
    @Override
    public int intValue() {
        return (int) doubleValue();
    }

    // Реализует абстрактный метод floatValue класса Number
    @Override
    public float floatValue() {
        return (float) doubleValue();
    }

    // Реализует абстрактный метод doubleValue класса Number
    @Override
    public double doubleValue() {
        return numerator.doubleValue() / denominator.doubleValue();
    }

    // Реализует абстрактный метод longValue класса Number
    @Override
    public long longValue() {
        return (long) doubleValue();
    }
}
