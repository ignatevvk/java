package abstractClass.task20;
/*

Задание №20

Напишите программу, которая запрашивает у пользователя ввести десятичное число, а отображает это число в виде дроби.

(Подсказка: считайте десятичное число как строку, извлеките целую и дробную часть строки и используйте реализацию
класса Rational с помощью BigInteger из задания № 19, чтобы получить рациональное число для десятичного.)

Примеры выполнения:

Введите десятичное число: 3.25
Это число в виде дроби равно 13/4

Введите десятичное число: -0.45452
Это число в виде дроби равно -11363/25000
 */

import java.math.BigInteger;
import java.util.Scanner;

public class TestBigInteger {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Введите десятичное число: ");
        String decimal = input.nextLine();

        System.out.println("Рациональное число равно " + getFraction(decimal));
    }

    private static Rational getFraction(String decimal) {
        String[] items = decimal.split("[.]");

        if (items.length == 1) {
            return new Rational(new BigInteger(items[0]), BigInteger.ONE);
        }
        else {
            Rational r = new Rational(new BigInteger(items[0] + items[1]), getDenominator(items[1].length()));
            return r;
        }
    }

    private static BigInteger getDenominator(int size) {
        BigInteger result = BigInteger.ONE;

        for (int i = 0; i < size; i++)
            result = result.multiply(new BigInteger("10"));

        return result;
    }

    static class Rational extends Number implements Comparable<Rational> {
        // Поля данных для числителя и знаменателя
        private BigInteger numerator = BigInteger.ZERO;
        private BigInteger denominator = BigInteger.ONE;

        /** Создает радиональное число с заданными по умолчанию свойствами */
        public Rational() {
            this(BigInteger.ZERO, BigInteger.ONE);
        }

        /** Создает радиональное число с указанным числителем и знаменателем */
        public Rational(BigInteger numerator, BigInteger denominator) {
            BigInteger gcd = gcd(numerator, denominator);

            if (denominator.compareTo(BigInteger.ZERO) < 0)
                this.numerator = numerator.multiply(new BigInteger("-1")).divide(gcd);
            else
                this.numerator = numerator.divide(gcd);
            this.denominator = denominator.abs().divide(gcd);
        }

        /** Находит НОД двух чисел */
        private static BigInteger gcd(BigInteger n, BigInteger d) {
            BigInteger n1 = n.abs();
            BigInteger n2 = d.abs();
            BigInteger gcd = BigInteger.ONE;

            for (BigInteger k = BigInteger.ONE;
                 k.compareTo(n1) <= 0 && k.compareTo(n2) <= 0;
                 k = k.add(BigInteger.ONE)) {
                if (n1.remainder(k).equals(BigInteger.ZERO) &&
                        n2.remainder(k).equals(BigInteger.ZERO))
                    gcd = k;
            }

            return gcd;
        }

        /** Возвращает числитель */
        public BigInteger getNumerator() {
            return numerator;
        }

        /** Возвращает знаменатель */
        public BigInteger getDenominator() {
            return denominator;
        }

        /** Прибавляет рациональное число к текущему */
        public Rational add(Rational secondRational) {
            BigInteger n = numerator.multiply(secondRational.getDenominator()).add(
                    denominator.multiply(secondRational.getNumerator()));
            BigInteger d = denominator.multiply(secondRational.getDenominator());
            return new Rational(n, d);
        }

        /** Вычитает рациональное число из текущего */
        public Rational subtract(Rational secondRational) {
            BigInteger n = numerator.multiply(secondRational.getDenominator()).subtract(
                    denominator.multiply(secondRational.getNumerator()));
            BigInteger d = denominator.multiply(secondRational.getDenominator());
            return new Rational(n, d);
        }

        /** Умножает рациональное число на текущее */
        public Rational multiply(Rational secondRational) {
            BigInteger n = numerator.multiply(secondRational.getNumerator());
            BigInteger d = denominator.multiply(secondRational.getDenominator());
            return new Rational(n, d);
        }

        /** Делит на рациональное число текущее */
        public Rational divide(Rational secondRational) {
            BigInteger n = numerator.multiply(secondRational.getDenominator());
            BigInteger d = denominator.multiply(secondRational.numerator);
            return new Rational(n, d);
        }

        @Override // Переопределяет метод toString класса Object
        public String toString() {
            if (denominator.equals(BigInteger.ONE))
                return numerator + "";
            else
                return numerator + "/" + denominator;
        }

        @Override // Переопределяет метод equals класса Object
        public boolean equals(Object parm1) {
            if ((this.subtract((Rational)(parm1))).getNumerator().equals(BigInteger.ONE))
                return true;
            else
                return false;
        }

        @Override // Переопределяет метод hashCode класса Object
        public int hashCode() {
            return new Double(this.doubleValue()).hashCode();
        }

        @Override // Реализует абстрактный метод intValue класса Number
        public int intValue() {
            return (int)doubleValue();
        }

        @Override // Реализует абстрактный метод floatValue класса Number
        public float floatValue() {
            return (float)doubleValue();
        }

        @Override // Реализует абстрактный метод doubleValue класса Number
        public double doubleValue() {
            return numerator.doubleValue() / denominator.doubleValue();
        }

        @Override // Реализует абстрактный метод longValue класса Number
        public long longValue() {
            return (long)doubleValue();
        }

        @Override // Реализует метод compareTo интерфейса Comparable
        public int compareTo(Rational o) {
            if ((this.subtract((Rational)o)).getNumerator().compareTo(BigInteger.ZERO) > 0)
                return 1;
            else if ((this.subtract((Rational)o)).getNumerator().compareTo(BigInteger.ZERO) < 0)
                return -1;
            else
                return 0;
        }
    }
}