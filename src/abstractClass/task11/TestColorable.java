package abstractClass.task11;

/*

Задание №11

Спроектируйте интерфейс с именем Colorable с помощью метода howToColor() типа void. Каждый класс раскрашиваемого
объекта должен реализовывать интерфейс Colorable. Спроектируйте класс Square, который наследуется от GeometricObject
 и реализует Colorable. Реализуйте метод howToColor() для отображения сообщения Раскрасьте все четыре стороны.
 Класс Square содержит поле данных side с getter- и setter-методами, а также конструктор для создания
 Square с указанной стороной. У класса Square есть скрытое поле данных типа double с именем side и getter-
 и setter-методами. У него есть безаргументный конструктор, который создает объект типа Square со стороной, равной 0,
  и еще один конструктор, который создает объект типа Square с указанной стороной.

Нарисуйте UML-диаграмму, которая включает в себя Colorable, Square и GeometricObject.
Напишите тестовую программу, которая создает массив из пяти объектов типа GeometricObjects.
 Для каждого объекта в массиве отобразите его площадь и вызовите метод howToColor(), если его можно раскрасить.
 */

import OOP.GeometricObject;

public class TestColorable {
    public static void main(String[] args) {
        GeometricObject[] objects = {new Square(2), new Circle(5), new Square(5), new Rectangle(3, 4), new Square(4.5)};

        for (int i = 0; i < objects.length; i++) {
            System.out.println("Площадь равна " + objects[i].getArea());
            if (objects[i] instanceof Colorable)
                ((Colorable)objects[i]).howToColor();
        }
    }
}

interface Colorable {
    void howToColor();
}

class Square extends GeometricObject implements Colorable {
    private double side;

    public Square(double side) {
        this.side = side;
    }

    @Override
    public void howToColor() {
        System.out.println("Раскрасьте все четыре стороны");
    }

    @Override
    public double getArea() {
        return side * side;
    }

    @Override
    public double getPerimeter() {
        return 4 * side;
    }
}
