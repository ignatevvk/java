package webinar07.oop1.task1;

/*
Включать/выключать люстру и показывать ее состояние
В люстре есть много ламп
 */
public class Chandelier {
    private Bulb[] chandelier;
    
    //Конструктор люстры - передаем количество лампочек и создаем объекты ламп в массив.
    public Chandelier(int countOfBulbs) {
        chandelier = new Bulb[countOfBulbs];
        
        for (int i = 0; i < countOfBulbs; i++) {
            chandelier[i] = new Bulb();
        }
    }
    //включение ламп в люстре
    public void turnOn() {
        for (Bulb bulb : chandelier) {
            bulb.turnOn();
        }
    }
    //выключение ламп в люстре
    public void turnOff() {
        for (Bulb bulb : chandelier) {
            bulb.turnOff();
        }
    }
    
    //придумать бизнес логику
    public boolean isShining() {
        return chandelier[0].isShining();
    }
}
