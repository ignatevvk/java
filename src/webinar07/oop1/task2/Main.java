package webinar07.oop1.task2;

public class Main {
    public static void main(String[] args) {
        //Thermometer thermometer = new Thermometer(-25, "F");
        Thermometer thermometer = new Thermometer(-25, TemperatureUnit.FAHRENHEIT);
        Thermometer thermometer1 = new Thermometer(-25, TemperatureUnit.CELSIUS);
        System.out.println("В цельсиях: " + thermometer.getTempCelsius());
        System.out.println("В Фаренгейтах: " + thermometer.getTempFahrenheit());
    
        System.out.println("В цельсиях: " + thermometer1.getTempCelsius());
        System.out.println("В Фаренгейтах: " + thermometer1.getTempFahrenheit());
    }
}
