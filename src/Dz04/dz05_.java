package Dz04;
/*
5. Сдвиг
На вход подается число N — длина массива.
Затем передается массив целых чисел (ai) из N элементов.
После этого передается число M — величина сдвига.

Необходимо циклически сдвинуть элементы массива на M элементов вправо.

Ограничение:
0 < N < 100
-1000 < ai < 1000
0 <= M < 100

Пример входных данных
5
38 44 0 -11 2
2

2 38 44 0 -11    первая итерация

Пример выходных данных
-11 2 38 44 0


 */

import java.util.Scanner;

public class dz05_ {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        double[] myList = new double[n];

        // заполняем массив
        for (int i = 0; i < myList.length; i++) {
            myList[i] = scanner.nextDouble();
        }

        int m = scanner.nextInt();
        // цикл смещает m раз
        for (int j = 1; j <= m; j++) {
            // смещаем массив на 1 в право
            double temp = myList[n - 1];
            for (int i = n - 1; i > 0; i--) {
                myList[i] = myList[i - 1];
            }
            myList[0] = temp;
        }
        // печать массива
        for (int i = 0; i < myList.length; i++) {
            System.out.print((int) myList[i] + " ");
        }
    }
}
