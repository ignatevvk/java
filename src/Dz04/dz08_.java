package Dz04;
/*
8. Найти ближайшее
На вход подается число N — длина массива. Затем передается массив целых чисел (ai) из N элементов.
После этого передается число M.

Необходимо найти в массиве число, максимально близкое к M (т.е. такое число, для которого |ai - M| минимальное).
Если их несколько, то вывести максимальное число.

Ограничение:
0 < N < 100
-1000 < ai < 1000
-1000 < M < 1000

Пример входных данных
6
-10 9 -5 -6 1 -3
-4

Пример выходных данных
-3
 */

import java.util.Scanner;

public class dz08_ {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        double[] myList = new double[n];
        for (int i = 0; i < myList.length; i++) {
            myList[i] = scanner.nextDouble();
        }
        int m = scanner.nextInt();
        double modul;
        double z = 1000;
        double minnum = myList[1];

        for (int i = 0; i < myList.length; i++) {

            modul = Math.abs((Math.abs(myList[i]) - Math.abs(m)));
            if (modul < z) {
                z = modul;
                minnum = myList[i];
            } else if (modul == z) {
                minnum = Math.max(myList[i], minnum);
            }
        }
        System.out.println((int) minnum);
    }
}
