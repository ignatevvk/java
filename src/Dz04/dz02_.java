package Dz04;
/*
2. Равенство массивов
На вход подается число N — длина массива.
Затем передается массив целых чисел (ai) из N элементов.
После этого аналогично передается второй массив (aj) длины M.

Необходимо вывести на экран true, если два массива одинаковы
(то есть содержат одинаковое количество элементов и для каждого i == j элемент ai == aj).
Иначе вывести false.

Ограничение:
0 < N < 100
0 < ai < 1000
0 < M < 100
0 < aj < 1000

Пример входных данных
7
1 2 3 4 5 6 7
7
1 2 3 4 5 6 7

Пример выходных данных
true
 */

import java.util.Scanner;

public class dz02_ {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        double[] myList1 = new double[n];
        for (int i = 0; i < myList1.length; i++) {
            myList1[i] = scanner.nextDouble();
        }

        int m = scanner.nextInt();
        double[] myList2 = new double[m];
        for (int j = 0; j < myList2.length; j++) {
            myList2[j] = scanner.nextDouble();
        }

        boolean res = false;

        if (myList1.length == myList2.length) {
            for (int i = 0; i < myList1.length; i++) {
                if (myList1[i] != myList2[i]) {
                    res = false;
                    break;
                } else {
                    res = true;
                }
            }
        }
        System.out.println(res);
    }
}


