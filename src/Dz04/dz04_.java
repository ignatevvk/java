package Dz04;
/*
4. Количество различных элементов
На вход подается число N — длина массива.
Затем передается массив целых чисел (ai) из N элементов, отсортированный по возрастанию.

Необходимо вывести на экран построчно сколько встретилось различных элементов.
Каждая строка должна содержать количество элементов и сам элемент через пробел.

Ограничение:
0 < N < 100
-1000 < ai < 1000

Пример входных данных
6
7 7 7 10 26 26

Пример выходных данных
3 7
1 10
2 26

 */

import java.util.Scanner;

public class dz04_ {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        double[] myList = new double[n];
        for (int i = 0; i < myList.length; i++) {
            myList[i] = scanner.nextDouble();
        }

        double indexStart = myList[0];
        //  double indexFinish = myList[n - 1];
        int col = 1;

        for (int i = 1; i < myList.length; i++) {
            if (indexStart == myList[i]) {
                col++;

            } else {
                System.out.println(col + " " + (int) indexStart);
                col = 1;
                indexStart = myList[i];
            }
        }
        System.out.println(col + " " + (int) indexStart);
    }
}



