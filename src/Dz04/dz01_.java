package Dz04;
/*
1. Посчитать среднее арифметическое
На вход подается число N — длина массива.
Затем передается массив вещественных чисел (ai) из N элементов.
Необходимо реализовать метод, который принимает на вход полученный массив и
возвращает среднее арифметическое всех чисел массива.
Вывести среднее арифметическое на экран.

Ограничение:
0 < N < 100
0 < ai < 1000

Пример входных данных
3
1.5 2.7 3.14

Пример выходных данных
2.4466666666666668
 */

import java.util.Scanner;

public class dz01_ {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double sum = 0;

        double[] myList = new double[n];

        for (int i = 0; i < myList.length; i++) {
            myList[i] = scanner.nextDouble();
            sum+=myList[i];
        }
        System.out.println(sum/n);
    }
}
