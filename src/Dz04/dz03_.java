package Dz04;
/*
3. Найти индекс
На вход подается число N — длина массива.
Затем передается массив целых чисел (ai) из N элементов, отсортированный по возрастанию.
После этого вводится число X — элемент, который нужно добавить в массив, чтобы сортировка в массиве сохранилась.

Необходимо вывести на экран индекс элемента массива, куда нужно добавить X.
Если в массиве уже есть число равное X, то X нужно поставить после уже существующего.

Ограничение:
0 < N < 100
-1000 < ai < 1000
-1000 < X < 1000

Пример входных данных
6
10 20 30 40 45 60
12

Пример выходных данных
1
 */

import java.util.Scanner;

public class dz03_ {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        double[] myList = new double[n];
        for (int i = 0; i < myList.length; i++) {
            myList[i] = scanner.nextDouble();
        }
        double x = scanner.nextInt();

        int min = 0;
        for (int i = 0; i < myList.length; i++) {
            if (x > myList[i]) {
                min = i + 1;
            }
            if (myList[i] == x) {
                min = i + 1;
            }
        }
        System.out.println(min);
    }
}
