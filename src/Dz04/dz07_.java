package Dz04;
/*
7. Возведение в квадрат
На вход подается число N — длина массива.
Затем передается массив целых чисел (ai) из N элементов, отсортированный по возрастанию.

Необходимо создать массив, полученный из исходного возведением в квадрат каждого элемента, упорядочить элементы по возрастанию и вывести их на экран.

Ограничение:
0 < N < 100
-1000 < ai < 1000

Пример входных данных
6
-10 -5 1 3 3 8

Пример выходных данных
1 9 9 25 64 100
 */

import java.util.Scanner;

public class dz07_ {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        double[] myList = new double[n];
        double[] myListFinish = new double[n];

        // заполняем массив
        for (int i = 0; i < myList.length; i++) {
            myList[i] = scanner.nextDouble();
            myListFinish[i] = Math.pow(myList[i], 2);
        }

        java.util.Arrays.sort(myListFinish);

        for (int i = 0; i < myListFinish.length; i++) {
            System.out.print((int) myListFinish[i] + " ");
        }
    }
}
