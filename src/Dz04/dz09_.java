package Dz04;
/*
9. Найти дубликат
На вход подается число N — длина массива.
Затем передается массив строк из N элементов (разделение через перевод строки).
Каждая строка содержит только строчные символы латинского алфавита.

Необходимо найти и вывести дубликат на экран.
Гарантируется что он есть и только один.

Ограничение:
0 < N < 100
0 < ai.length() < 1000

Пример входных данных
4
hello
java
hi
java

Пример выходных данных
java
 */

import java.util.Scanner;

public class dz09_ {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        String[] myList = new String[n];
        for (int i = 0; i < myList.length; i++) {
            myList[i] = scanner.next();
        }

        String key = myList[0];
        String z = null;

        for (int i = 0; i < myList.length; i++) {
            key = myList[i];

            // сравниваем каждый элемент со всеми остальными
            for (int j = 0; j < myList.length; j++) {
                if (i == j) continue; // не проверяем элемент с собой же

                if (key.equals(myList[j])) {
                    // дубликат найден, сохраним его
                    z = key;
                }
            }
        }
        System.out.println(z);
    }
}










