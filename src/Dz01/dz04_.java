package Dz01;

import java.util.Scanner;

/*
 На вход подается количество секунд, прошедших с начала текущего дня – count.
 Выведите в консоль текущее время в формате: часы и минуты.

 */
public class dz04_ {


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int sec = scanner.nextInt();
        int hour = sec / (60 * 60);
        int min = sec / 60 % 60;

        System.out.print(hour + " " + min);
    }
}


