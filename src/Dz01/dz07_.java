package Dz01;

import java.util.Scanner;

/*
На вход подается двузначное число n. Выведите число, полученное
перестановкой цифр в исходном числе n. Если после перестановки получается
ведущий 0, его также надо вывести.

 */
public class dz07_ {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String n = scanner.next();

        System.out.println(n.charAt(1) + ""+ n.charAt(0));
    }
}
