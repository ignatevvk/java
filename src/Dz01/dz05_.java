package Dz01;

import java.util.Scanner;

public class dz05_ {
    static final double INCH_IN_CENTIMETR = 2.54;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int inch = scanner.nextInt();

        System.out.println(inch * INCH_IN_CENTIMETR);
    }
}
