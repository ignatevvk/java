package Dz01;

import java.util.Scanner;

/*
Вычислите и выведите на экран объем шара, получив его радиус r с консоли.
Подсказка: считать по формуле V = 4/3 * pi * r^3.
Значение числа pi взять из Math.
 */
public class dz01_ {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int r = sc.nextInt();
        double v = 4 / 3.0 * Math.PI * Math.pow(r, 3);
        System.out.println(v);
    }
}

