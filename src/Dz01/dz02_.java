package Dz01;

import java.util.Scanner;

/*
На вход подается два целых числа a и b. Вычислите и выведите среднее
квадратическое a и b
 */
public class dz02_ {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();

        double c = Math.sqrt(1.0 / 2 * (Math.pow((double) a, 2) + Math.pow((double) b, 2)));
        System.out.println("Cреднее квадратическое равно: " + c);
    }
}
