package Dz01;

import java.util.Scanner;

public class dz06_ {
    static final double MILE_IN_KILOMETR = 1.60934;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();

        System.out.println("Равно " + count / MILE_IN_KILOMETR + " миль.");
    }
}
