package dz01prof.task01;

public class Learning {
    private final boolean moduleBase;
    private final boolean moduleProf;

    public Learning(String name, boolean moduleBase, boolean moduleProf) {
        this.moduleBase = moduleBase;
        this.moduleProf = moduleProf;
    }

    public void check() throws MyCheckedException {
        if (moduleBase && moduleProf) {
            System.out.println("Обучение завершено");
        } else {
            throw new MyCheckedException();
        }
    }

    public static void main(String[] args) throws MyCheckedException {
        Learning learning = new Learning("СберУниверситет", true, true);
        learning.check();
        Learning learning1 = new Learning("СберУниверситет", true, false);
        //learning1.check();
        try {
            learning1.check();
        } catch (MyCheckedException e) {
            System.out.println(e.getMessage());
        }
    }
}

