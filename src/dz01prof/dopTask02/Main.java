package dz01prof.dopTask02;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        int[] arr = new int[n];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = scanner.nextInt();
        }

        int p = scanner.nextInt();

        int firstIndex = 0;
        int lastIndex = arr.length - 1;
        boolean flag = true;

        while (firstIndex <= lastIndex) {

            int middleIndex = (firstIndex + lastIndex) / 2;

            if (arr[middleIndex] == p) {
                System.out.println(middleIndex);
                flag = false;
                break;
            } else if (arr[middleIndex] < p) {
                firstIndex = middleIndex + 1;
            } else if (arr[middleIndex] > p) {
                lastIndex = middleIndex - 1;
            }
        }
        if (flag) {
            System.out.println(-1);
        }
    }
}


