package dz01prof.task06;

public enum Gender {
    MALE("Male"),
    FEMALE("Female");

    public final String name;

    Gender(String name) {
        this.name = name;
    }
}
