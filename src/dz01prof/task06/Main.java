package dz01prof.task06;

public class Main {
    public static void main(String[] args) throws Exception {

        // Тест на валидацию имени
        FormValidator.checkName("Ivanov");

        // Тест на валидацию даты
        FormValidator.checkBirthdate("10.12.1982");

        // Тест на пол
        FormValidator.checkGender("male");

        //Тест на рост
        FormValidator.checkHeight("180.5");
    }
}
