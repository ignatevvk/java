package dz01prof.task06;

import java.time.LocalDate;

public class FormValidator {

    private FormValidator() {
    }

    public static void checkName(String str) throws Exception {
        // По условию задачи нет ограничений на имя (кроме первой буквы и длинны), но т.к. речь идет о человеке,
        // сделал более жесткую валидацию имени.
        //Ещё варианты:
        // str.matches("[A-Z][^-^]{1,19}");
        // str.matches("[A-Z][a-zA-Z0-9]{1,19}");

        if (str.matches("[A-Z][a-z]{1,19}")) {
            System.out.println("Валидация имени прошла успешно!");
        } else {
            throw new Exception("Неверный формат имени");
        }
    }

    public static void checkBirthdate(String str) throws Exception {

        DateValidator dateValidator = new DateValidator();
        LocalDate firstDate = LocalDate.of(1900, 1, 1);
        LocalDate secondDate = LocalDate.now();

        if (dateValidator.validate(str)) {
            String str1 = str.substring(6, 10) + "-" + str.substring(3, 5) + "-" + str.substring(0, 2);
            LocalDate date = LocalDate.parse(str1);

            if (date.isAfter(firstDate) && date.isBefore(secondDate)) {
                System.out.println("Валидация даты прошла успешно!");
            } else {
                throw new Exception("Дата находится за пределами допустимого диапазона (Текущее время - 01.01.1900)");
            }
        } else {
            throw new Exception("Неверный формат даты, введите в формате XX.XX.XXXX");
        }
    }

    public static void checkGender(String str) throws Exception {
        if (Gender.MALE.name.equalsIgnoreCase(str) || Gender.FEMALE.name.equalsIgnoreCase(str)) {
            System.out.println("Валидация на пол прошла успешно!");
        } else {
            throw new Exception("Пол введён неверно!");
        }
    }

    public static void checkHeight(String str) {
        try {
            double height = Double.parseDouble(str);
            if (height > 0) {
                System.out.println("Валидация роста прошла успешно!");
            } else {
                throw new Exception("Рост должен быть больше 0!");
            }
        } catch (Exception e) {
            System.out.println("Неверный формат строки (рост)");
        }
    }
}

