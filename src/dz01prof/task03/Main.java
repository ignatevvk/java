package dz01prof.task03;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        try {
            ReadAndWriteFile.readAndWriteData();
        } catch (IOException e) {
            System.out.println("FileReadWrite#main!error: " + e.getMessage());
        }
    }
}
