package dz01prof.task03;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Scanner;

public class ReadAndWriteFile {
    private static final String FOLDER_DIRECTORY = "C:\\Users\\Vyacheslav\\IdeaProjects\\test01\\src\\dz01prof\\task03\\files";

    private ReadAndWriteFile() {
    }

    public static void readAndWriteData() throws IOException {
        Scanner scanner = new Scanner(new File(FOLDER_DIRECTORY + "\\input.txt"));
        Writer writer = new FileWriter(FOLDER_DIRECTORY + "\\output.txt");

        try (scanner; writer) {
            while (scanner.hasNextLine()) {
                String res = scanner.nextLine().toUpperCase() + "\n";
                writer.write(res);
            }
        }
    }
}
