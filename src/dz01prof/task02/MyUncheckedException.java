package dz01prof.task02;

public class MyUncheckedException extends ArrayIndexOutOfBoundsException {
    public MyUncheckedException() {
        super("Индекс элемента выходит за массив. Печать невозможна!");
    }

    public MyUncheckedException(String message) {
        super(message);
    }
}
