package dz01prof.task02;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3};
        printElementArr(1, arr);
        printElementArr(3, arr);
    }

    public static void printElementArr(int i, int[] arr) {
        try {
            System.out.println(arr[i]);
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new MyUncheckedException();
            //throw new MyUncheckedException(e.getMessage());
        }
    }
}
