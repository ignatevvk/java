package dz03prof.task01dop;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        System.out.println("Проверки на true: ");
        System.out.println(CheckString(""));
        System.out.println(CheckString("(()()()())"));
        System.out.println(CheckString("((()))"));
        System.out.println();

        System.out.println("Проверки на false: ");
        System.out.println(CheckString("55"));
        System.out.println(CheckString("(()"));
        System.out.println(CheckString(")("));
    }

    private static boolean CheckString(String str) {
        if (str.isEmpty()) {
            System.out.println("[Пустая строка]");
            return true;
        }

        final char start = '(';
        final char end = ')';
        Character[] arr = new Character[str.length()];

        for (int i = 0; i < str.length(); i++) {
            arr[i] = str.charAt(i);
        }
        System.out.println(Arrays.toString(arr));

        if (!arr[0].equals(start) || !arr[str.length() - 1].equals(end)) {
            return false;
        }

        int startSymbol = 0;
        int endSymbol = 0;

        for (Character character : arr) {
            if (character == start) {
                startSymbol++;
            } else if (character == end) {
                endSymbol++;
            } else {
                return false;
            }
        }
        return startSymbol == endSymbol;
    }
}


