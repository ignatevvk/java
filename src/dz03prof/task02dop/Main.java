package dz03prof.task02dop;

import java.util.Arrays;
import java.util.Stack;

public class Main {
    public static void main(String[] args) {

        System.out.println("Проверки на true: ");
        System.out.println(checkString(" "));
        System.out.println(checkString("{()[]()}"));
        System.out.println(checkString("[{(){}}][()]{}"));
        System.out.println();

        System.out.println("Проверки на false: ");
        System.out.println(checkString(")("));
        System.out.println(checkString("{)(}"));
        System.out.println(checkString("[}"));
        System.out.println(checkString("[(]{})"));
    }

    private static boolean checkString(String str) {
        if (str.isEmpty()) {
            System.out.println("[Пустая строка]");
            return true;
        }

        char[] arr = str.toCharArray();
        System.out.println(Arrays.toString(arr));

        Stack<Character> stack = new Stack<>();

        for (char element : arr) {
            if (element == '{' || element == '[' || element == '(') {
                stack.push(element);
            } else {
                boolean b = (!stack.isEmpty() && ((stack.peek().equals('{')) && element == '}'
                        || (stack.peek().equals('[') && element == ']')
                        || (stack.peek().equals('(') && element == ')')));
                {
                    if (b) {
                        stack.pop();
                    } else {
                        return false;
                    }
                }
            }
        }
        return stack.isEmpty();
    }
}
