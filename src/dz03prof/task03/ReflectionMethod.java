package dz03prof.task03;
/* С помощью рефлексии вызвать метод print() и обработать все
 возможные ошибки (в качестве аргумента передавать любое подходящее
 число). При “ловле” исключений выводить на экран краткое описание ошибки.
 */

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class ReflectionMethod {
    public static void main(String[] args) {

        Class<APrinter> aPrinterClazz = APrinter.class;

        try {
            Constructor<APrinter> constructor = aPrinterClazz.getDeclaredConstructor();
            APrinter objAPrint = constructor.newInstance();
            objAPrint.print(555);
        } catch (NoSuchMethodException e) {
            System.out.println("Метод объекта не обнаружен! описание: " + e.getMessage());
        } catch (InvocationTargetException e) {
            System.out.println("Класс для вызова не обнаружен! описание: " + e.getMessage());
        } catch (InstantiationException e) {
            System.out.println("Невозможно создать объект класса! описание: " + e.getMessage());
        } catch (IllegalAccessException e) {
            System.out.println("Доступ ограничен! описание: " + e.getMessage());
        }
    }
}
