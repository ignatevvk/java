package dz03prof.task02;
/*
Написать метод, который рефлексивно проверит наличие аннотации @IsLike на
любом переданном классе и выведет значение, хранящееся в аннотации, на
экран.
 */

import dz03prof.task01.IsLike;
import dz03prof.task01.MyClass;

public class Reflection {
    public static void main(String[] args) {
        Class<MyClass> c = MyClass.class;
        printAnnotation(c);
        printAnnotation(String.class);
    }

    public static void printAnnotation(Class<?> clazz) {
        try {
            IsLike classIsLike = clazz.getAnnotation(IsLike.class);
            System.out.println("Значение value: " + classIsLike.value());
        } catch (NullPointerException e) {
            throw new NullPointerException("Класс не имеет аннотации");
            //  System.out.println("Класс не имеет аннотации");
        }
    }
}

