package dz03prof.task04;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReflectionSuperInterface {

    public static void main(String[] args) {

        List<Class<?>> result = getAllInterfaces(ClassA.class);
        List<Class<?>> result1 = getOldInterfaces(result);
        result.addAll(result1);

        for (Class<?> clazz : result) {
            System.out.println(clazz.getName());
        }
    }

    public static List<Class<?>> getAllInterfaces(Class<?> clazz) {
        List<Class<?>> interfaces = new ArrayList<>();
        while (clazz != Object.class) {
            interfaces.addAll(Arrays.asList(clazz.getInterfaces()));
            interfaces.addAll(Arrays.asList(interfaces.getClass().getInterfaces()));
            clazz = clazz.getSuperclass();
        }
        return interfaces;
    }

    public static List<Class<?>> getOldInterfaces(List<Class<?>> result) {
        List<Class<?>> temp = result;
        List<Class<?>> temp1 = new ArrayList<>();
        List<Class<?>> interfacesTemp = new ArrayList<>();
        boolean flag = true;

        while (flag) {
            flag = false;
            for (Class<?> clazz : temp) {
                if (clazz.getInterfaces().length != 0) {
                    interfacesTemp.addAll(Arrays.asList(clazz.getInterfaces()));
                    temp1.addAll(Arrays.asList(clazz.getInterfaces()));
                    flag = true;
                }
            }
            temp = temp1;
            temp1.clear();
        }
        return interfacesTemp;
    }
}


