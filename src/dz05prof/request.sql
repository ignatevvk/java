/*
  1. По идентификатору заказа получить данные заказа и данные клиента,
создавшего этот заказ
 */
select orders.id, orders.flowers, orders.quantity, orders.date, name
from shoppers
         join orders on
    shoppers.id = orders.shopper_id
where orders.id = 3;

/*
 2. Получить данные всех заказов одного клиента по идентификатору
клиента за последний месяц
 */
select *
from orders
where shopper_id = 1
  and date > current_date - interval '1 months';

/*
 3. Найти заказ с максимальным количеством купленных цветов, вывести их
название и количество
 */
select flowers, orders.quantity
from orders
where quantity = (select max(quantity) from orders);

/*
 4. Вывести общую выручку (сумму золотых монет по всем заказам) за все
время
 */
select sum(flowers.price * orders.quantity) as result
from flowers
         join orders on flowers.name = orders.flowers;