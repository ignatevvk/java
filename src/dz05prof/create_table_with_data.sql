/*
Заполнение таблици цветов
 */
insert into flowers(name, price)
values ('Розы', 100);

insert into flowers(name, price)
values ('Лилии', 50);

insert into flowers(name, price)
values ('Ромашки', 25);

-- для тестирования мне
select *
from flowers;

drop table flowers;

/*
Заполнение таблици покупателей
 */
insert into shoppers(name, phone)
values ('Эвелина', +79056321542);

insert into shoppers(name, phone)
values ('Лилит', +79056321543);

insert into shoppers(name, phone)
values ('Артем', +79056321544);

-- для тестирования мне
select *
from shoppers;

/*
Заполнение таблици заказов
 */
insert into orders(id, shopper_id, flowers, quantity, date)
values (1, 1, 'Лилии', 5, now());

insert into orders(id, shopper_id, flowers, quantity, date)
values (2, 1, 'Розы', 7, now());

insert into orders(id, shopper_id, flowers, quantity, date)
values (3, 2, 'Розы', 3, now() - INTERVAL '1 DAY');

-- для тестирования мне
drop table orders;

select *
from orders;


