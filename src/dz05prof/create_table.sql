/*
 Создать базу данных покупателей
 */
create table shoppers
(
    id    serial primary key,
    name  varchar(30) not null,
    phone varchar(12) not null
);

/*
Создать базу данных цветов
 */
create table flowers
(
    id    serial primary key,
    name  varchar(30) not null
        constraint flowers_name_key unique,
    price integer     not null
);

/*
 Создать базу данных заказов
 */
create table orders
(
    id         serial primary key,
    shopper_id integer references shoppers (id),
    flowers    varchar(30) references flowers (name),
    quantity   integer   not null check ( orders.quantity > 0 and orders.quantity < 1000 ),
    date       timestamp not null
);



