package dz02prof.task01;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        ArrayList<Integer> arr = new ArrayList<>();
        arr.add(0);
        arr.add(1);
        arr.add(2);
        arr.add(3);
        arr.add(2);
        arr.add(3);

        System.out.println(ReturnUniqueValues.convert(arr));
    }
}
