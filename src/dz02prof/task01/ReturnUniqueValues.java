package dz02prof.task01;

import java.util.ArrayList;
import java.util.HashSet;

public class ReturnUniqueValues {

    private ReturnUniqueValues() {
    }

    public static <T> HashSet<T> convert(ArrayList<T> elements) {
        return new HashSet<>(elements);
    }
}



