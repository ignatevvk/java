package dz02prof.task02;

public class Main {
    public static void main(String[] args) {

        String str1 = "Иван";
        String str2 = "навИ";
        System.out.println(ValidationOfTwoStrings.anagram(str1,str2));

        String str3 = "Иван Ivanov";
        String str4 = "навb Ivanov";
        System.out.println(ValidationOfTwoStrings.anagram(str3,str4));

    }
}
