package dz02prof.task02;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ValidationOfTwoStrings {

    private ValidationOfTwoStrings() {
    }

    public static boolean anagram(String str1, String str2) {
        if (str1.length() != str2.length()) {
            return false;
        }

        List<Character> str1arr = new ArrayList<>();
        for (int i = 0; i < str1.length(); i++) {
            str1arr.add(str1.charAt(i));
        }
        List<Character> str2arr = new ArrayList<>();
        for (int i = 0; i < str2.length(); i++) {
            str2arr.add(str2.charAt(i));
        }
        Collections.sort(str1arr);
        Collections.sort(str2arr);
        System.out.println(str1arr);
        System.out.println(str2arr);

        return str1arr.equals(str2arr);
    }
}
