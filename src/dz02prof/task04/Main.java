package dz02prof.task04;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {

        List<Document> documents = new ArrayList<>();

        documents.add(new Document(3,"C++",200));
        documents.add(new Document(1,"Java",100));
        documents.add(new Document(2,"Pascal",150));

        Map<Integer,Document> map = OrganizeDoc.organizeDocuments(documents);
        System.out.println(map);
    }
}
