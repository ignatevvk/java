package dz02prof.task04;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrganizeDoc {

    private OrganizeDoc() {
    }

    public static Map<Integer, Document> organizeDocuments(List<Document> documents) {

        Map<Integer, Document> documentMap = new HashMap<>();
        for (Document element : documents) {
            documentMap.put(element.id, element);
        }
        return documentMap;
    }
}
