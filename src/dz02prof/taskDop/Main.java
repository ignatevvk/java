package dz02prof.taskDop;

import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        String[] words = {"the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is", "day", "one", "one"};
        int k = 4;

        Map<String, Integer> map = new HashMap<>();

        for (String value : words) {
            int counter = 1;
            if (map.containsKey(value)) {
                counter = map.get(value);
                counter++;
            }
            map.put(value, counter);
        }

        System.out.println(map);

        // Сортировка Map по значению
        Map<String, Integer> sortedMap = map.entrySet().stream()
                .sorted(Comparator.comparingInt(e -> -e.getValue()))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (a, b) -> {
                            throw new AssertionError();
                        },
                        LinkedHashMap::new
                ));

        System.out.println(sortedMap);

        String[] words1 = new String[k];
        int i = 0;

        // Заполним новый массив длинной к
        for (String key : sortedMap.keySet()) {
            if (i < 4) {
                words1[i] = key;
                i++;
            } else {
                break;
            }
        }

        System.out.println(Arrays.toString(words1));
    }
}
