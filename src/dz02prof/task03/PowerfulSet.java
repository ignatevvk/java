package dz02prof.task03;

import java.util.HashSet;
import java.util.Set;

public class PowerfulSet {

    private PowerfulSet() {
    }

    // Возвращает пересечение двух наборов. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}. Вернуть {1, 2}
    public static <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
        Set<T> temp = new HashSet<>();

        for (T element : set1) {
            for (T element1 : set2) {
                if (element.equals(element1)) {
                    temp.add(element);
                }
            }
        }
        return temp;
    }

    // Возвращает объединение двух наборов. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}. Вернуть {0, 1, 2, 3, 4}

    public static <T> Set<T> union(Set<T> set1, Set<T> set2) {
        Set<T> temp = new HashSet<>();
        temp.addAll(set1);
        temp.addAll(set2);
        return temp;
    }

    //Возвращает элементы первого набора без тех, которые находятся также и во втором наборе.
    // Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}. Вернуть {3}

    public static <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) {
        Set<T> temp = new HashSet<>();
        for (T element : set1) {
            if (!set2.contains(element)) {
                temp.add(element);
            }
        }
        return temp;
    }
}

