package webinar10;

import webinar10.xmltags.XmlFirstTag;

public interface IXmlFileStructure {
    
    XmlFirstTag fillFirstTag();
    
    void fillSecondTag();
    
    void fillNTag();
}
