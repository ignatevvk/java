package generics.task21;
/*
Задание №21
Измените класс GenericStack таким образом, чтобы реализовать его с помощью массива, а не ArrayList.
 Перед добавлением нового элемента в стек необходимо проверить размер массива. Если массив заполнен,
 создайте новый массив, который удвоит текущий размер массива и скопирует элементы из текущего массива в новый.

 */

class GenericStack<E> {
    public final static int INITIAL_SIZE = 16;
    private E[] elements;
    private int size;

    /** Создает стек с заданной по умолчанию исходной емкостью */
    public GenericStack() {
        this(INITIAL_SIZE);
    }

    /** Создает стрек с указанной исходной емкостью */
    public GenericStack(int initialCapacity) {
        elements = (E[])new Object[initialCapacity];
    }

    /** Добавляет новый элемент на вершину стека */
    public E push(E value) {
        if (size >= elements.length) {
            E[] temp = (E[])new Object[elements.length * 2];
            System.arraycopy(elements, 0, temp, 0, elements.length);
            elements = temp;
        }

        return elements[size++] = value;
    }

    /** Возвращает и удаляет верхний элемент из стека */
    public E pop() {
        return elements[--size];
    }

    /** Возвращает верхний элемент из стека */
    public E peek() {
        return elements[size - 1];
    }

    /** Проверяет, пустой ли стек */
    public boolean isEmpty() {
        return size == 0;
    }

    /** Возвращает количество элементов в стеке */
    public int getSize() {
        return size;
    }
}