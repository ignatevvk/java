package generics.task22;
/*
Задание №22
Класс GenericStack из описания предыдущего задания реализован с помощью отношения композиции.
Определите новый класс стека, который наследуется от ArrayList. Нарисуйте UML-диаграмму этих классов,
а затем реализуйте новый класс GenericStack. Напишите тестовую программу, которая запросит у пользователя пять строк,
а отобразит их в обратном порядке.

 */

public class TestGenericStack {
    public static void main(String[] args) {
        GenericStack<String> stack = new GenericStack<String>();
        stack.push("Вера");
        stack.push("Надежда");
        stack.push("Любовь");
        System.out.println(stack.getSize());
        System.out.println(stack.peek());
        System.out.println(stack.pop());
        System.out.println(stack.peek());
    }

    static class GenericStack<E> extends java.util.ArrayList<E> {
        public boolean isEmpty() {
            return super.isEmpty();
        }

        public int getSize() {
            return size();
        }

        public Object peek() {
            return get(getSize() - 1);
        }

        public Object pop() {
            Object o = get(getSize() - 1);
            remove(getSize() - 1);
            return o;
        }

        public Object push(E o) {
            add(o);
            return o;
        }

        public int search(Object o) {
            return indexOf(o);
        }

        @Override
        public String toString() {
            return "стек: " + toString();
        }
    }
}
