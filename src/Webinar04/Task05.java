package Webinar04;
/*
Дано целое число n, n > 0. Вывести сумму всех цифр этого числа.
92180 -> 20 //9 + 2 + 1 + 8 + 0 == 20

 */

import java.util.Scanner;

public class Task05 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // моё решение
        int n = scanner.nextInt();
        int sum = 0;

        do {
            sum = sum + n % 10;
            n = n / 10;

        } while (n > 0);
        System.out.println(sum);

        //через преобразование в строку:
        /*
        sum += Integer.parseInt(String.valueOf(num.charAt(i)));
         */

    }
}
