package Webinar04;

/*
Даны числа m < 13 и n < 7.
Вывести все степени (от 0 до n включительно) числа m с помощью цикла.
3 6
->
1
3
9
27
81
243
729
 */

import java.util.Scanner;

public class Task03 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();

      /*
        // вариант 1
         for (int i = 0; i <= n; i++) {
              System.out.println((int) Math.pow(m, i));
        }

        //вариант 2
        int res = 1;
        System.out.println(res);
        for (int i = 1; i <= n; i++) {
            res = res * m;
            System.out.println(res);
            }


        //вариант 3
        int res = 1;
        System.out.println(res);
        int i = 1;

        while (i <= n) {
            res*=m;
            System.out.println(res);
            i++;
        }
    */
        //вариант 4
        int res = 1;
        int i = 1;
        do {
            System.out.println(res);
            ;
            res *= m;
            i++;
        } while (i <= n + 1);
    }
}

