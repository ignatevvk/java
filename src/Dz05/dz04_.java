package Dz05;
/*
4. Удаление из матрицы
На вход подается число N — количество строк и столбцов матрицы.
Затем передается сама матрица, состоящая из натуральных чисел.
После этого передается натуральное число P.

Необходимо найти элемент P в матрице и удалить столбец и строку его содержащий
(т.е. сохранить и вывести на экран массив меньшей размерности).
Гарантируется, что искомый элемент единственный в массиве.

Ограничение:
0 < N < 100
0 < ai < 1000

Пример входных данных
3
1 2 3
1 7 3
1 2 3


7

Пример выходных данных
1 3
1 3
 */

import java.util.Scanner;

public class dz04_ {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[][] matrix = new int[n][n];

        int[][] matrixFinish = new int[n - 1][n - 1]; // создадим матрицу меньшего размера

        // заполняем матрицу
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = scanner.nextInt();
            }
        }

        int p = scanner.nextInt();      // получаем элемент для поиска

        // индексы найденого элемента
        int x = 0;
        int y = 0;

        // ищим элемент
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] == p) {
                    x = i;
                    y = j;
                }
            }
        }

        // заполняем новый массив
        int i1 = 0;
        for (int i = 0; i < matrixFinish.length; i++) {
            if (i1 == x) {
                i1 = i1 + 1; // пропускаем если индекс равен мндексу метки
            }
            int j1 = 0;
            for (int j = 0; j < matrixFinish[i].length; j++) {
                if (j1 == y) {
                    j1 = j1 + 1; // пропускаем если индекс равен мндексу метки
                }
                matrixFinish[i][j] = matrix[i1][j1];
                j1++;
            }
            i1++;
        }

        // Печатаем результат
        for (int i = 0; i < matrixFinish.length; i++) {
            for (int j = 0; j < matrixFinish[i].length; j++) {
                if (j < matrixFinish[i].length - 1) {
                    System.out.print(matrixFinish[i][j] + " ");
                } else {
                    System.out.print(matrixFinish[i][j]);
                }
            }
            System.out.println();
        }
    }
}















