package Dz05;
/*
3. Шахматы
На вход подается число N — количество строк и столбцов матрицы.
Затем передаются координаты X и Y расположения коня на шахматной доске.

Необходимо заполнить матрицу размера NxN нулями, местоположение коня отметить символом K,
а позиции, которые он может бить, символом X.

Ограничение:
4 < N < 100
0 <= X, Y < N

Пример входных данных
5
0 5

Пример выходных данных
0 0 0 0 0
0 0 0 0 0
0 X 0 0 0
0 0 X 0 0
K 0 0 0 0
 */

import java.util.Scanner;

public class dz03_ {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt(); //N — количество столбцов в двумерном массиве
        int x = scanner.nextInt();
        int y = scanner.nextInt();

        char[][] bord = new char[n][n];

        // Заполняем 0
        for (int i = 0; i < bord.length; i++) {
            for (int j = 0; j < bord[i].length; j++) {
                bord[i][j] = '0';
            }
        }

        //выводим позицию коня
        bord[y][x] = 'K';

        int[] dx = {-2, -1, 1, 2, 2, 1, -1, -2 }; // дельта по координатам по x
        int[] dy = { 1, 2, 2, 1, -1, -2, -2, -1}; // дельта по координатам по x

      // проход по 8 позициям с проверкой выхода за границы доски
        for (int i = 0; i < 8; i++) {
            if ((x + dx[i] >= 0 && x + dx[i] <= n - 1) && (y + dy[i] >= 0 && y + dy[i] <= n - 1)) {
                bord[y + dy[i]][x + dx[i]] = 'X';

            }
        }

        // Печатаем результат
        for (int i = 0; i < bord.length; i++) {
            for (int j = 0; j < bord[i].length; j++) {
                if (j < bord[i].length - 1) {
                    System.out.print(bord[i][j] + " ");
                } else {
                    System.out.print(bord[i][j]);
                }
            }
            System.out.println();
        }
    }
}
