package Dz05;
/*
6. Подсчет калорий
Петя решил начать следить за своей фигурой.
Но все существующие приложения для подсчета калорий ему не понравились и он решил написать свое.
Петя хочет каждый день записывать сколько белков, жиров, углеводов и калорий он съел, а в конце недели приложение
должно его уведомлять, вписался ли он в свою норму или нет.

На вход подаются числа A — недельная норма белков, B — недельная норма жиров, C — недельная норма углеводов и
K — недельная норма калорий.
Затем передаются 7 строк, в которых в том же порядке указаны сколько было съедено Петей нутриентов в каждый день недели.
Если за неделю в сумме по каждому нутриенту не превышена недельная норма, то вывести “Отлично”, иначе вывести
“Нужно есть поменьше”.

Ограничение:
0 < A, B, C < 2000
0 < ai, bi, ci < 2000
0 < K < 20000
0 < ki < 20000

Пример входных данных
882 595 1232 17500
116 85 76 2300
100 98 124 2500
182 70 154 2750
114 85 74 1900
96 77 60 1890
110 96 98 2500
155 67 124 2500

Пример выходных данных
Отлично
 */

import java.util.Scanner;

public class dz06_ {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int c = scanner.nextInt();
        int d = scanner.nextInt();
        int k = scanner.nextInt();

        int sumA = 0;
        int sumC = 0;
        int sumD = 0;
        int sumK = 0;

        int[][] matrix = new int[7][4];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = scanner.nextInt();
            }
        }

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                switch (j) {
                    case 0:
                        sumA = sumA + matrix[i][j];
                        break;
                    case 1:
                        sumC = sumC + matrix[i][j];
                        break;
                    case 2:
                        sumD = sumD + matrix[i][j];
                        break;
                    case 3:
                        sumK = sumK + matrix[i][j];
                        break;
                    default:
                        break;
                }
            }
        }
        if (sumA < a && sumC < c && sumD < d && sumK < k) {
            System.out.println("Отлично");
        } else {
            System.out.println("Нужно есть поменьше");
        }
    }
}

