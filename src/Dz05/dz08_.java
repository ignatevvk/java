package Dz05;
/*
8. Сумма цифр
На вход подается число N.
Необходимо посчитать и вывести на экран сумму его цифр.
Решить задачу нужно через рекурсию.

Ограничение:
0 < N < 1000000

Пример входных данных
12374

Пример выходных данных
17
 */

import java.util.Scanner;

public class dz08_ {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(sumNum(n));
    }

    private static int sumNum(int n) {
        if (n / 10 != 0)
            return n % 10 + sumNum(n / 10);
        else
            return n % 10;
    }
}


