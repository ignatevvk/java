package Dz05;
/*
9. Вывести цифры
На вход подается число N.
Необходимо вывести цифры числа слева направо.
Решить задачу нужно через рекурсию.

Ограничение:
0 < N < 1000000

Пример входных данных
12374

Пример выходных данных
1 2 3 7 4
 */

import java.util.Scanner;

public class dz09_ {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        invertNum(n);
    }

    private static void invertNum(int n) {
        if (n / 10 == 0) {
            System.out.print(n + " ");
        } else {
            invertNum(n / 10);
            System.out.print(n % 10 + " ");
        }
    }
}
