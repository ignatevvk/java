package Dz05;
/*
1. Минимальный элемент
На вход передается N — количество столбцов в двумерном массиве и M — количество строк.
Затем сам передается двумерный массив, состоящий из натуральных чисел.

Необходимо сохранить в одномерном массиве и вывести на экран минимальный элемент каждой строки.

Ограничение:
0 < N < 100
0 < M < 100
0 < ai < 1000

Пример входных данных
3 2
10 20 15
7 5 9

Пример выходных данных
 10 5
 */

import java.util.Scanner;

public class dz01_ {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int column = scanner.nextInt(); //N — количество столбцов в двумерном массиве
        int row = scanner.nextInt();    //M — количество строк


        int[][] matrix = new int[row][column];

        for (row = 0; row < matrix.length; row++) {
            for (column = 0; column < matrix[row].length; column++) {
                matrix[row][column] = scanner.nextInt();
            }
        }

        for (row = 0; row < matrix.length; row++) {
            int minnum = matrix[row][0];
            for (column = 0; column < matrix[row].length; column++) {
                minnum = Math.min(minnum, matrix[row][column]);

            }
            System.out.print(minnum + " ");
        }
    }
}
