package Dz05;
/*
10. Обратный порядок
На вход подается число N.
Необходимо вывести цифры числа справа налево.
Решить задачу нужно через рекурсию.

Ограничение:
0 < N < 1000000

Пример входных данных
12374

Пример выходных данных
4 7 3 2 1
 */

import java.util.Scanner;

public class dz10_ {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        invertNum(n);
    }

    private static int invertNum(int n) {
        System.out.print(n % 10 + " ");
        if (n >= 10) {
            invertNum(n / 10);
        }
        return n;
    }
}


