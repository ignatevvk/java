package Dz05;
/*
2. Нарисовать прямоугольник
На вход подается число N — количество строк и столбцов матрицы.
Затем в последующих двух строках подаются координаты X (номер столбца) и Y (номер строки) точек, которые задают прямоугольник.

Необходимо отобразить прямоугольник с помощью символа 1 в матрице, заполненной нулями (см. пример) и вывести всю матрицу на экран.

Ограничение:
0 < N < 100
0 <= X1, Y1, X2, Y2 < N
X1 < X2
Y1 < Y2

Пример входных данных
7
1 (x1) 2 (y1)
3 (x2) 4 (y2)

Пример выходных данных
0 0 0 0 0 0 0
0 0 0 0 0 0 0
0 1 1 1 0 0 0
0 1 0 1 0 0 0
0 1 1 1 0 0 0
0 0 0 0 0 0 0
0 0 0 0 0 0 0
 */

import java.util.Scanner;

public class dz02_ {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt(); //N — количество столбцов в двумерном массиве
        int x = n;
        int y = n;

        int x1 = scanner.nextInt();
        int y1 = scanner.nextInt();
        int x2 = scanner.nextInt();
        int y2 = scanner.nextInt();

        int[][] matrix = new int[y][x];

        // Заполняем 0
        for (y = 0; y < matrix.length; y++) {
            for (x = 0; x < matrix[y].length; x++) {
                matrix[y][x] = 0;
            }
        }

        // заполним по x оси единицами
        for (y = 0; y < matrix.length; y++) {
            if ((y == y1) || (y == y2)) {
                for (x = x1; x <= x2; x++) {
                    matrix[y][x] = 1;
                }
            }

            // рисуем вертикальные линии из единиц
            if (y > y1 && y < y2) {
                for (x = x1; x <= x2; x++) {
                    if (x == x1 || x == x2) {
                        matrix[y][x] = 1;
                    }
                }
            }
        }
        // Печатаем результат
        for (y = 0; y < matrix.length; y++) {
            for (x = 0; x < matrix[y].length; x++) {
                if (x < matrix[y].length - 1) {
                    System.out.print(matrix[y][x] + " ");
                } else {
                    System.out.print(matrix[y][x]);
                }
            }
            System.out.println();

      }
    }
}
