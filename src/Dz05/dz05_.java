package Dz05;
/*
5. Проверить симметричность
На вход подается число N — количество строк и столбцов матрицы.
Затем передается сама матрица, состоящая из натуральных чисел.

Необходимо вывести true, если она является симметричной относительно побочной диагонали, false иначе.

Побочной диагональю называется диагональ, проходящая из верхнего правого угла в левый нижний.

Ограничение:
0 < N < 100
0 < ai < 1000

Пример входных данных
3
1 2 3
4 5 6
7 8 9

1 2 3
4 5 2
7 4 1

Пример выходных данных
false

5

57 190 160 71 42
141 79 187 19 71
141 16 7 187 160
100 42 16 79 190
15 100 141 141 57

 true

 */

import java.util.Scanner;

public class dz05_ {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[][] arr = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr[i][j] = scanner.nextInt();
            }
        }

        boolean flag = false;
        int k = 1; // итерируем каждую строчку чтобы не доходить до диагонали

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n - k; j++) {
                if (arr[i][j] == arr[n - 1 - j][n - 1 - i]) { // во втором случаи индексы i и j поменяли местами
                    flag = true;
                } else {
                    flag = false;
                }
               // System.out.print(arr[i][j] + " ");  Для контроля
               // System.out.println(arr[n - 1 - j][n - 1 - i] + "-");  Для контроля
            }
            k++;
        }
        System.out.println(flag);
    }
}