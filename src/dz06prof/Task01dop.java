package dz06prof;
/*
Напишите программу для проверки, является ли введение число - числом Армстронга
Число Армстронга — натуральное число, которое в данной системе счисления равно сумме
своих цифр, возведенных в степень, равную количеству его цифр. Иногда, чтобы считать
число таковым, достаточно, чтобы степени, в которые возводятся цифры, были равны m.
Например, десятичное число
153— число Армстронга, потому что
1^3 + 5^3 + 3^3 = 153.
 */

public class Task01dop {
    public static void main(String[] args) {

        System.out.println(checkNum(1));
        System.out.println(checkNum(9));
        System.out.println(checkNum(153));
        System.out.println(checkNum(407));
        System.out.println(checkNum(24678051));

        System.out.println("++++++++++++++++");
        System.out.println(checkNum(200));
        System.out.println(checkNum(267780));
    }

    private static boolean checkNum(Integer num) {
        if (num <= 0) {
            return false;
        } else {
            int k = num.toString().length();
            long sum = 0;
            int z = num;
            for (int i = 0; i < k; i++) {
                sum = (long) (sum + Math.pow(z % 10, k));
                z = z / 10;
            }
            return num == sum;
        }
    }
}
