package dz06prof;
/*
Напишите программы, чтобы узнать, является ли введенное число простым или нет.
Просто́е число́ — натуральное число, имеющее ровно два различных натуральных делителя. Другими словами,
 натуральное число p p является простым, если оно отлично от 1 и делится без остатка только на 1  и на само p p[1].
2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109,
 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199,
 */

public class Task02dop {
    public static void main(String[] args) {
        System.out.println(checkNum(7));
        System.out.println(checkNum(41));
        System.out.println(checkNum(103));
        System.out.println("+++++++++++++");
        System.out.println(checkNum(1));
        System.out.println(checkNum(25));
        System.out.println(checkNum(110));
    }

    private static boolean checkNum(Integer num) {
        if (num <= 1) {
            return false;
        }
        for (int i = 2; i < num; i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }
}
