package dz21;
/*
Решить задачу 7 основного дз за линейное время.

На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию.
Необходимо создать массив, полученный из исходного возведением в квадрат
каждого элемента, упорядочить элементы по возрастанию и вывести их на
экран.
Ограничения:
● 0 < N < 100
● -1000 < ai < 1000

Пример входных данных
6
-10 -5 1 3 3 8

3
1 2 3
-5 -5 0
-5 -4 -4

Пример выходных данных
1 9 9 25 64 100

 */

import java.util.Scanner;

public class Dz12 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] arr = new int[n];
        int[] test = new int[1];
        int[] myListFinish = new int[n];

        // заполняем массив
        for (int i = 0; i < arr.length; i++) {
            arr[i] = scanner.nextInt();
        }

        // массив получен и заполнен, необходимо решить за линейное время,
        // т.е.нужно выполнить сортировку используя только один цикл, тогда будет O(n) "линейное время"
        // возведение в степень и сортировка в одном цикле без вложенных.

        int i = 0;       // для цикла while 0->n-1
        int k = n - 1;   //обратный проход по первому массиву n-1 -> 0
        int z = n - 1;   //обратный проход по финишнуму массиву n-1 -> 0
        int j = 0;       // проход по стартовым значениям
        int x = 0;       // прямой проход финишного массива

        while ((i < n - 1) && (j < n)) {  // добавил условия для индекса j, без него была ошибка выхода индекса
            int startNum = arr[j];
            if (startNum < 0) {
                if (Math.abs(startNum) > arr[k]) {
                    myListFinish[z] = (int) Math.pow(Math.abs(startNum), 2);
                    z--;
                    j++;
                    if (j < n) {  //// добавил условия для индекса j
                        startNum = arr[j];
                    }
                } else {
                    myListFinish[z] = (int) Math.pow((arr[k]), 2);
                    z--;
                    k--;
                }
            } else {
                if (myListFinish[x] != test[0]) {
                    break;
                } else {
                    myListFinish[x] = (int) Math.pow((arr[j]), 2);
                    x++;
                    j++;
                }
            }
        }

        // выводим на печать
        for (int v = 0; v < myListFinish.length; v++) {
            System.out.print((int) myListFinish[v] + " ");
        }
    }
}
