package dz21;
/*
Создать программу генерирующую пароль.
На вход подается число N — длина желаемого пароля. Необходимо проверить,
что N >= 8, иначе вывести на экран "Пароль с N количеством символов
небезопасен" (подставить вместо N число) и предложить пользователю еще раз
ввести число N.
Если N >= 8 то сгенерировать пароль, удовлетворяющий условиям ниже и
вывести его на экран. В пароле должны быть:
● заглавные латинские символы //26
● строчные латинские символы  //26
● числа                       //10
● специальные знаки(_*-)      //3
 */

import java.util.*;

public class dz11_1 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        // проверка на длинну пароля
        int n;
        do {
            n = scanner.nextInt();
            if (n < 8) {
                System.out.println("Пароль с " + n + " количеством символов небезопасен.");
            }
        } while (n < 8);

        // создадим общий массив символов длянной 65 (3 спец знак+ 26 завглавных букв + 26 строчных букв + 10 чисел)
        char[] allSymbol = new char[65];

        //заполним массив от 0 до 3 спец символами
        allSymbol[0] = '_';
        allSymbol[1] = '*';
        allSymbol[2] = '-';

        // заполняем массив от 3 до 28 заглавными буквами
        char k1 = 'A';
        for (int i = 3; i < 29; i++) {
            allSymbol[i] = k1;
            k1++;
        }
        // заполняем массив от 29 до 54 строчными буквами
        char k2 = 'a';
        for (int i = 29; i < 55; i++) {
            allSymbol[i] = k2;
            k2++;
        }
        // заполняем массив от 55 до 64 строчными буквами
        char k3 = '0';
        for (int i = 55; i < 65; i++) {
            allSymbol[i] = k3;
            k3++;
        }

        // создадим массив для pass длинной n
        char[] pass = new char[n];

        // заполним первых четыри символы, для соблюдения условий безопастности
        pass[0] = allSymbol[(int) (Math.random() * 3)];
        pass[1] = allSymbol[(int) (3 + Math.random() * 26)];
        pass[2] = allSymbol[(int) (29 + Math.random() * 26)];
        pass[3] = allSymbol[(int) (55 + Math.random() * 10)];

        // случайным образом заполняем оставшиеся элементы массива
        for (int i = 4; i < pass.length; i++) {
            pass[i] = allSymbol[(int) (Math.random() * 65)];
        }

        /* Не перемешанный массив, для контроля мне.
        for (int i = 0; i < pass.length; i++) {
            System.out.print(pass[i]);
        }
        System.out.println();
         */

        // Перемешаем массив с паролем, возможно еще через shuffle(), но это позже.
        Random rnd = new Random();
        for (int i = 0; i < pass.length; i++) {
            int index = rnd.nextInt(i + 1);
            int a = pass[index];
            pass[index] = pass[i];
            pass[i] = (char) a;
        }

        // Выводим секурный пароль
        for (int i = 0; i < pass.length; i++) {
            System.out.print(pass[i]);
        }
    }
}

