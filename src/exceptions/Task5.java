package exceptions;
/*

Задание №5

Напишите программу, которая запрашивает у пользователя два целых числа и отображает их сумму.
В случае некорректного ввода необходимо его обработать и повторно запросить у пользователя числа.
 */

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        boolean done = false;
        int number1 = 0;
        int number2 = 0;

        // Получить два целых числа
        System.out.print("Введите два целых числа: ");

        while (!done) {
            try {
                number1 = input.nextInt();
                number2 = input.nextInt();
                done = true;
            }
            catch (Exception ex) {
                ex.printStackTrace();
                System.out.print("Некорректный ввод! Введите повторно два целых числа: ");
                input.nextLine(); // пропуск входных данных
            }
        }

        System.out.println("Сумма равна " + (number1 + number2));
    }
}
