package OOP.exceptions.task4;
/*
Задание 4

В программе из задания № 22 в разделе «Наследование и полиморфизм» был определен класс Triangle с тремя сторонами.
 В треугольнике сумма длин любых двух сторон больше длины третьей стороны.
 Класс Triangle должен удовлетворять этому правилу. Создайте класс IllegalTriangleException и
 измените конструктор класса Triangle, чтобы выбросить объект типа IllegalTriangleException,
 если треугольник создан со сторонами, нарушающими это правило, следующим образом:

 Создает треугольник с указанными сторонами
      public Triangle(double side1, double side2, double side3)
        throws IllegalTriangleException {
        // Реализуйте его
        }
 */

public class TestTriangleWithException {
    public static void main(String[] args) {
        try {
            TriangleWithException t1 = new TriangleWithException(1.5, 2, 3);
            System.out.println("Периметр t1: " + t1.getPerimeter());
            System.out.println("Площадь t1: " + t1.getArea());

            TriangleWithException t2 = new TriangleWithException(1, 2, 3);
            System.out.println("Периметр t2: " + t2.getPerimeter());
            System.out.println("Площадь t2: " + t2.getArea());
        }
        catch (IllegalTriangleException ex) {
            System.out.println("Недопустимый треугольник");
            System.out.println("Сторона 1: " + ex.getSide1());
            System.out.println("Сторона 2: " + ex.getSide2());
            System.out.println("Сторона 3: " + ex.getSide3());
        }
    }
}

class IllegalTriangleException extends Exception {
    private double side1, side2, side3;

    public IllegalTriangleException(double side1,
                                    double side2, double side3, String s) {
        super(s);
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    public double getSide1() {
        return side1;
    }

    public double getSide2() {
        return side2;
    }

    public double getSide3() {
        return side3;
    }
}

// Назовите новый класс Triangle как TrianlgeWithException, чтобы избежать
// конфликта имен с уже определенным классом Triangle
class TriangleWithException extends Object {
    double side1, side2, side3;

    /** Конструктор */
    public TriangleWithException(double side1, double side2, double side3)
            throws IllegalTriangleException {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
        if (side1 + side2 <= side3 || side1 + side3 <= side2 || side2 + side3 <= side1)
            throw new IllegalTriangleException(side1, side2, side3,
                    "Сумма двух сторон больше третьей");
    }

    /** Реализует абстрактный метод findArea класса GeometricObject */
    public double getArea() {
        double s = (side1 + side2 + side3) / 2;
        return Math.sqrt(s * (s - side1) * (s - side2) * (s - side3));
    }

    /** Реализует абстрактный метод findCircumference класса GeometricObject **/
    public double getPerimeter() {
        return side1 + side2 + side3;
    }

    @Override
    public String toString() {
        return "Треугольник: сторона 1 = " + side1 + " сторона 2 = " + side2 +
                " сторона 3 = " + side3;
    }
}