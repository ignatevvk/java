package exceptions;
/*
Задание 2
Программа из задания №1 нормально работает до тех пор, пока пользователь вводит целое число.
В противном случае вы можете получить другой тип исключения. Например,
если вы используете метод nextInt() класса Scanner, то у вас может произойти InputMismatchException.
Измените программу таким образом, чтобы предотвратить ввод пользователем любого числа, кроме целого.
 */

import java.util.InputMismatchException;
import java.util.Scanner;

public class DaysInMonthWithProcessing {
    private static final String[] months = {"январь", "февраль", "март", "апрель", "май",
            "июнь", "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь"};

    private static final int[] dom = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    /**
     * Метод main
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        while (true) {
            System.out.print("Введите порядковый номер месяца, от 1 до 12: ");

            try {
                // Получить порядковый номер месяца
                int monthNumber = input.nextInt();
                // Отобразить название и количество дней в соответствующем месяце
                System.out.println("Месяц: " + months[monthNumber - 1]);
                System.out.println("Количество дней: " + dom[monthNumber - 1]);
            } catch (InputMismatchException | ArrayIndexOutOfBoundsException e) {
                // Отобразить сообщение если введен неверный номер месяца
                System.out.println("Недопустимое число");
                // Считать символ новой строчки, созданный нажатием «Enter» после числа
                input.nextLine();
            }
        }
    }
}
