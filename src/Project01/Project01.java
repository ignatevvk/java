package Project01;
/*
Напишите программу, которая конвертирует сумму денег из американских долларов
в российские рубли по курсу покупки 72.12.

Мои данные:
ФУНТЫ СТРЕЛЕНГИ
 */

import java.util.Scanner;

public class Project01 {
  public static void main(String[] args) {
    final double ROUBLES_PER_GBP = 85.38; 
    
    int gbp; 
    double roubles; 
    
    Scanner input = new Scanner(System.in);
    
    // Получить сумму денег в фунтах стерлингах.
       System.out.print("Введите cумму денег в фунтах стерлингах: ");
       gbp = input.nextInt();
    
    // Конвертировать сумму денег в российские рубли.
       roubles = ROUBLES_PER_GBP * gbp;
       
    // Отобразить сумму денег в российских рублях в пользу покупателя.
       System.out.println("Она равна " + (int) (roubles *100) / 100.0 + " российского рубля.");
  } 
}
