package Project01;

import java.util.Scanner;
/*
Перепишите программу, которая конвертирует сумму денег из американских долларов
в российские рубли по курсу покупки 72.12, добавив структуру выбора
для принятия решений об окончаниях входной валюты в зависимости от ее значения.

Мои данные:
ФУНТЫ СТРЕЛЕНГИ
 */

public class Project02 {
  public static void main(String[] args) {
    final double ROUBLES_PER_GBP = 85.38; 
    
    int gbp, digit; 
    double roubles; 
    
    Scanner input = new Scanner(System.in);
    
    // Получить сумму денег в фунтах стерлингах.
       System.out.print("Введите cумму денег в фунтах стерлингах: ");
         gbp = input.nextInt();
       
    // Отобразить сумму денег в фунтах стерлингах с правильным окончанием.
       System.out.print(gbp);
       if (5 <= gbp && gbp <= 20) {
         System.out.print(" фунтов стерлингов равно "); 
       } else {
           digit = gbp % 10;
             if (digit == 1) 
               System.out.print(" фунт стерлинг равно "); 
           else if (2 <= digit && digit <= 4)
               System.out.print(" фунта стерлинга равно "); 
           else
               System.out.print(" фунтов стерлингов равно "); 
       }    
    
    // Конвертировать сумму денег в российские рубли.
       roubles = ROUBLES_PER_GBP * gbp;
       
    // Отобразить сумму денег в российских рублях в пользу покупателя.
       System.out.println((int) (roubles *100) / 100.0 + " российского рубля.");
  } 
}
