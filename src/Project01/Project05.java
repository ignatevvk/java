package Project01;
/*
Перепишите программу, которая конвертирует сумму денег из американских долларов
в российские рубли по курсу покупки 72.12, добавив одномерные массивы
сумм денег для многократных конвертаций.

Мои данные:
ФУНТЫ СТРЕЛЕНГИ
 */

import java.util.Scanner;

public class Project05 {
    static final double ROUBLES_PER_GBP = 85.38;

    public static void main(String[] args) {

        int[] gbp;
        double[] roubles;
        int i, n;

        Scanner input = new Scanner(System.in);

        //Отобразить инструкцию.
        instruct();

        //Получать количество конвертаций до тех пор, пока не введено корректное значение
        do {
            System.out.print("Введите корректное количество конвертаций: ");
            n = input.nextInt();
        } while (n <= 0);

        // Получить n сумму денег в фунтах стерлингах.
        System.out.print("Введите " + n + " cумму денег в фунтах стерлингах: ");
        gbp = new int[n];
        for (i = 0; i < n; ++i) {
            gbp[i] = input.nextInt();
        }

        // Конвертировать n сумму денег в российские рубли.
        roubles = find_roubles(gbp, n);

        // Отобразить в таблице n сумму денег в российских рублях в пользу покупателя.
        System.out.println("\n    Сумма,GBP  Сумма, Pуб");
        for (i = 0; i < n; ++i)
            System.out.println("\t" + gbp[i] + "\t       " + (int) (roubles[i] * 100) / 100.0);
    }

    // Отображение инструкции
    public static void instruct() {
        System.out.println("Эта программа конвертирует сумму денег "
                + "из фунтов стерлингов в российские рубли.");
        System.out.println("Курс покупки равен " + ROUBLES_PER_GBP);
    }

    // Конвертируем n сумму из фунтов стерлингов в российские рубли
    public static double[] find_roubles(int[] gbp, int n) {
        double[] roubles = new double[n];
        int i;
        for (i = 0; i < n; ++i) {
            roubles[i] = ROUBLES_PER_GBP * gbp[i];
        }
        return roubles;
    }
}
