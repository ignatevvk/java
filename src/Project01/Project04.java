package Project01;
/*
Перепишите программу, которая конвертирует сумму денег из американских долларов
в российские рубли по курсу покупки 72.12, добавив метод, отображающий инструкции,
и метод, конвертирующий сумму денег.

Мои данные:
ФУНТЫ СТРЕЛЕНГИ
 */

import java.util.Scanner;

public class Project04 {
    static final double ROUBLES_PER_GBP = 85.38;

    public static void main(String[] args) {

        int gbp, digit;
        double roubles;
        int i, n;

        Scanner input = new Scanner(System.in);

        //Отобразить инструкцию.
        instruct();

        //Получать количество конвертаций до тех пор, пока не введено корректное значение
        do {
            System.out.print("Введите корректное количество конвертаций: ");
            n = input.nextInt();
        } while (n <= 0);

        /*До тех пор, пока не конвертированы все суммы, получать, отображать и
         конвертировать суммы денег в фунты стрелинги и отображать суммы денег в российских рублях.
         */
        for (i = 0; i < n; ++i) {

            // Получить сумму денег в фунтах стерлингах.
            System.out.print("Введите cумму денег в фунтах стерлингах: ");
            gbp = input.nextInt();

            // Отобразить сумму денег в фунтах стерлингах с правильным окончанием.
            System.out.print(gbp);
            if (5 <= gbp && gbp <= 20) {
                System.out.print(" фунтов стерлингов равно ");
            } else {
                digit = gbp % 10;
                if (digit == 1)
                    System.out.print(" фунт стерлинг равно ");
                else if (2 <= digit && digit <= 4)
                    System.out.print(" фунта стерлинга равно ");
                else
                    System.out.print(" фунтов стерлингов равно ");
            }

            // Конвертировать сумму денег в российские рубли.
            roubles = find_roubles(gbp);

            // Отобразить сумму денег в российских рублях в пользу покупателя.
            System.out.println((int) (roubles * 100) / 100.0 + " российского рубля.");
        }
    }

    // Отображение инструкции
    public static void instruct() {
        System.out.println("Эта программа конвертирует сумму денег "
                + "из фунтов стерлингов в российские рубли.");
        System.out.println("Курс покупки равен " + ROUBLES_PER_GBP);
    }

    // Конвертируем сумму из фунтов стерлингов в российские рубли
    public static double find_roubles(int gbp) {
        return ROUBLES_PER_GBP * gbp;
    }

}
