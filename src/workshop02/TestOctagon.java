package workshop02;

public class TestOctagon {
    public static void main(String[] args) throws CloneNotSupportedException {
        // Создать новый объект класса Octagon с заданной стороной
        Octagon originalOctagon = new Octagon(5);

        // Отобразить информацию о площади
        System.out.println("Площадь равна: " + originalOctagon.getArea());
        // Отобразить информацию о периметре
        System.out.println("Периметр равен: " + originalOctagon.getPerimeter());

        Octagon cloneOctagon = null;
        try {
            cloneOctagon = (Octagon) originalOctagon.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println("Объект не может быть клонирован");
        }

        // Сравнить два объекта
        int comparisonResult = originalOctagon.compareTo(cloneOctagon);

        // Отобразить результат сравнения двух объектов
        System.out.println("Результат сравнения объектов: " + comparisonResult);
    }
}
