package workshop02;
/*
Напишите класс с именем Octagon (восьмиугольник), который наследуется от GeometricObject и реализует
 интерфейсы Comparable и Cloneable. Предположим, что все восемь сторон восьмиугольника имеют одинаковую длину.
 Площадь (area) можно вычислить по следующей формуле:

У класса Octagon есть private-поле типа double с именем side и соответствующие ему getter- и setter-методы.
Также у этого класса есть безаргументный конструктор для создания восьмиугольника со стороной, равной 0,
и конструктор для создания восьмиугольника с указанной стороной.

Нарисуйте UML-диаграмму, включающую Octagon, GeometricObject, Comparable и Cloneable. Напишите тестовую программу,
которая создает объект типа Octagon со значением side, равным 5, и отображает его площадь и периметр.
Создайте новый объект с помощью метода clone() и сравните два объекта с помощью метода compareTo().
 */

import OOP.GeometricObject;

import static java.lang.Math.sqrt;

public class Octagon extends GeometricObject implements Comparable<Octagon>, Cloneable {
    private double side;

    public Octagon() {
        this.side = 0;
    }
    public Octagon(double side) {
        this.side = side;
    }


    /** Возвращает длину стороны */
    public double getSide() {
        return side;
    }

    /** Присваивает длину стороны */
    public void setSide(double side) {
        this.side = side;
    }



    @Override
    public double getArea() {
        return (2 + 4 / sqrt(2)) * side * side;
    }

    @Override
    public double getPerimeter() {
        return 8 * side;
    }

    @Override
    public int compareTo(Octagon o) {
        return Double.compare(this.side, o.side);
    }

    /** Переопределяет метод класса Object */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
