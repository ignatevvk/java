package dz33.task03;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        List<Integer> arr = new ArrayList<>();
        for (int i = 0; i < m * 2; i++) {
            arr.add(i);
        }
        int z = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(arr.get(j + z) + " ");
            }
            System.out.println();
            z++;
        }
    }
}
