package dz33.task01;

public class Main {
    public static void main(String[] args) {

        Bat bat = new Bat();
        bat.getInfo();
        bat.getType();
        bat.getBorne();
        bat.getCanEat();
        bat.getCanSleep();
        bat.skill();
        bat.speed();
        System.out.println();

        Dolphin dolphin = new Dolphin();
        dolphin.getInfo();
        dolphin.getType();
        dolphin.getBorne();
        dolphin.getCanEat();
        dolphin.getCanSleep();
        dolphin.skill();
        dolphin.speed();
        System.out.println();

        GoldFish goldFish = new GoldFish();
        goldFish.getInfo();
        goldFish.getType();
        goldFish.getBorne();
        goldFish.getCanEat();
        goldFish.getCanSleep();
        goldFish.skill();
        goldFish.speed();
        System.out.println();

        Eagle eagle = new Eagle();
        eagle.getInfo();
        eagle.getType();
        eagle.getBorne();
        eagle.getCanEat();
        eagle.getCanSleep();
        eagle.skill();
        eagle.speed();
    }
}
