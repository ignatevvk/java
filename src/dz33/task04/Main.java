package dz33.task04;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        int n = input.nextInt();

        List<Participant> arrParticipants = new ArrayList<>();

        System.out.println("Введите имена владельцев собак:");
        for (int i = 0; i < n; i++) {
            arrParticipants.add(new Participant(input.next()));
        }
        System.out.println("Введите имена собак:");
        for (int i = 0; i < n; i++) {
            arrParticipants.get(i).setDog(new Dog(input.next()));
        }
        System.out.println("Введите оценки:");
        for (int i = 0; i < n; i++) {
            int[] temp = new int[3];
            for (int j = 0; j < temp.length; j++) {
                temp[j] = input.nextInt();
            }
            arrParticipants.get(i).getDog().addBalls(temp);
        }

        arrParticipants.sort(new Comparator<Participant>() {
            @Override
            public int compare(Participant o1, Participant o2) {
                return Double.compare(o2.getDog().getAverageScore(), o1.getDog().getAverageScore());
            }
        });

        for (int i = 0; i < 3; i++) {
            System.out.print(arrParticipants.get(i).getName() + ": " + arrParticipants.get(i).getDog().getName() + ", ");
            System.out.printf("%.1f%n", arrParticipants.get(i).getDog().getAverageScore());
        }
    }
}




