package dz33.task04;

public class Dog {
    private final String name;
    private double averageScore;

    public Dog(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public double getAverageScore() {
        return averageScore;
    }

    public void addBalls(int[] balls) {
        int sum = 0;

        for (int ball : balls) {
            sum = sum + ball;
        }
        this.averageScore = (double) sum / 3;
    }
}
