package dz33.task04;

public class Participant {
    private final String name;

    private Dog dog;

    public Participant(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Dog getDog() {
        return dog;
    }

    public void setDog(Dog dog) {
        this.dog = dog;
    }
}
