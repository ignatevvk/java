package dz33.task02;

public class BestCarpenterEver {

    private final Stool stool = new Stool("Стул");

    public BestCarpenterEver() {
    }

    public boolean getRepair(Furniture furniture) {
        if (stool.getClass().equals(furniture.getClass())) {
            System.out.println("Мы сможем починить данную мебель.");
            return true;
        } else {
            System.out.println("Данную мебель мы не чиним.");
            return false;
        }
    }
}
