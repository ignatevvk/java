package dz33.task02;

public class Main {
    public static void main(String[] args) {

        Furniture furniture1 = new Stool("Стул");
        Furniture furniture2 = new Table("Стол");

        BestCarpenterEver bestCarpenterEver = new BestCarpenterEver();

        System.out.println(bestCarpenterEver.getRepair(furniture1));
        System.out.println(bestCarpenterEver.getRepair(furniture2));
    }
}
