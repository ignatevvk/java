package webinar09.oop3.icecreamfactory;

public enum IceCreamType {
    CHERRY,
    CHOCOLATE,
    VANILLA
}
